﻿using Data;
using Microsoft.EntityFrameworkCore;
using Models.Entities;
using System;
using System.Linq;

namespace Services
{
    public class ToDoListService
    {
        private readonly ToDoDBContext _dbContext;

        public ToDoListService(ToDoDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void GetToDoList()
        {
            int currentUserId = UserService.currentUser.Id;
            var listToDos = _dbContext.ToDoLists.Include(l => l.Users).Where(l => l.Users.Any(u => u.Id == currentUserId)).ToList();

            if (listToDos.Count > 0)
            {
                listToDos.ForEach(list => Console.WriteLine($"{{ id = {list.Id}, \n Title = {list.Title}, \n CretedDate = {list.CreatedDate}, \n CreatorId = {list.CreatorId}, \n ModifiedId = {list.ModifierId}, \n ModifiedDate =  {list.ModifiedDate}}}"));
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"User doesn't have access to list.");
                Console.ResetColor();
            }
        }

        public void CreateList()
        {
            int creatorId = UserService.currentUser.Id;

            Console.WriteLine("Input Title for the ToDo List");
            Console.Write("Title: ");
            string listTitle = Console.ReadLine();

            AddNewList(listTitle, creatorId);
        }

        public void EditList()
        {
            Console.WriteLine("Input ID for the List you are editing.");
            Console.Write("Id: ");
            int id;
            bool validId = Int32.TryParse(Console.ReadLine(), out id);

            if (!validId || id <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid id.");
                Console.ResetColor();
                return;
            }
            else
            {
                Console.Write("Title: ");
                string title = Console.ReadLine();

                EditList(id, title);
            }
        }

        public void DeleteList()
        {
            Console.WriteLine("Input ID for the List you want to delete.");
            Console.Write("Id: ");
            int id;
            bool validId = Int32.TryParse(Console.ReadLine(), out id);

            if (!validId || id <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid id.");
                Console.ResetColor();
                return;
            }

            RemoveList(id);
        }

        public void ShareList()
        {
            Console.WriteLine("Input ID for the List you want to share.");
            Console.Write("List Id: ");
            int listId;
            bool validListId = Int32.TryParse(Console.ReadLine(), out listId);
            Console.WriteLine("Input ID of the User you want to share the list.");
            Console.Write("User Id: ");
            int userId;
            bool validId = Int32.TryParse(Console.ReadLine(), out userId);

            if (!validListId || listId <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid List id.");
                Console.ResetColor();
                return;
            }
            if (!validId || userId <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid User id.");
                Console.ResetColor();
                return;
            }

            ShareList(listId, userId);
        }

        public bool AddNewList(string title, int creatorId)
        {
            int currentUserId = UserService.currentUser.Id;
            var currentUser = _dbContext.Users.Find(currentUserId);

            ToDoList newList = new ToDoList(title, creatorId);
            newList.Users.Add(currentUser);

            _dbContext.ToDoLists.Add(newList);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("List was created.");
            Console.ResetColor();

            _dbContext.SaveChanges();
            return true;
        }

        public bool EditList(int id, string title)
        {
            int currentUserId = UserService.currentUser.Id;
            ToDoList editList = _dbContext.ToDoLists.Include(l => l.Users).SingleOrDefault(l => l.Id == id);

            if (editList == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("List does not exist.");
                Console.ResetColor();
                return false;
            }
            else if (!editList.Users.Any(u => u.Id == currentUserId))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User does not have permission to edit list.");
                Console.ResetColor();
                return false;
            }
            else
            {
                editList.Title = title;
                editList.ModifiedDate = DateTime.Now;
                editList.ModifierId = currentUserId;
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"List with id: {id} is edited.");
            Console.ResetColor();

            UserService.currentUser.ModifiedLists.Add(editList);
            _dbContext.ToDoLists.Update(editList);
            _dbContext.SaveChanges();
            return true;
        }

        public bool RemoveList(int id)
        {
            int currentUserId = UserService.currentUser.Id;
            ToDoList removedList = _dbContext.ToDoLists.Include(l => l.Users).SingleOrDefault(l => l.Id == id);

            if (removedList == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("List does not exist.");
                Console.ResetColor();
                return false;
            }
            else if (!removedList.Users.Any(u => u.Id == currentUserId))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User does not have permission to delete list.");
                Console.ResetColor();
                return false;
            }
            else if (removedList.CreatorId == currentUserId)
            {
                _dbContext.ToDoLists.Remove(removedList);
            }
            else
            {
                removedList.Users.Remove(UserService.currentUser);
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"List id: {id} is removed.");
            Console.ResetColor();

            _dbContext.SaveChanges();
            return true;
        }

        public bool ShareList(int listId, int userId)
        {
            int currentUserId = UserService.currentUser.Id;
            ToDoList sharedList = _dbContext.ToDoLists.Include(l => l.Users).SingleOrDefault(l => l.Id == listId);
            bool userExists = _dbContext.Users.Any(u => u.Id == userId);

            if (sharedList == null || !userExists)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User or List does not exist.");
                Console.ResetColor();
                return false;
            }
            else if (!sharedList.Users.Any(u => u.Id == currentUserId))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User is unauthorised to share this list.");
                Console.ResetColor();
                return false;
            }
            else if (sharedList.Users.Any(u => u.Id == userId))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("List is already shared.");
                Console.ResetColor();
                return false;
            }

            sharedList.ModifierId = UserService.currentUser.Id;
            sharedList.ModifiedDate = DateTime.Now;

            var user = _dbContext.Users.Find(userId);
            sharedList.Users.Add(user);
            UserService.currentUser.ModifiedLists.Add(sharedList);
            _dbContext.ToDoLists.Update(sharedList);
            _dbContext.SaveChanges();

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"List is shared to User with id: {userId}.");
            Console.ResetColor();
            return true;
        }
    }
}
