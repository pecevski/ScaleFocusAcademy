﻿using Data;
using Models.Entities;
using Models.Enums;
using System;
using System.Linq;

namespace Services
{
    public class UserService
    {
        public static bool signed = false;

        public static User currentUser;

        private readonly ToDoDBContext _dbContext;

        public UserService(ToDoDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public User Login(string username, string password)
        {
            try
            {
                var user = _dbContext.Users.SingleOrDefault(u => u.Username == username && u.Password == password);
                if (user == null)
                {
                    signed = false;
                    return null;
                }

                if (user.Password == password)
                {
                    signed = true;
                    currentUser = user;
                    return user;
                }

                signed = false;
                return null;
            }
            catch (Exception)
            {
                signed = false;
                return null;
            }
        }

        public void ListOfUsers()
        {
            var users = _dbContext.Users.ToList();

            if (users.Count > 0)
            {
                foreach (var user in users)
                {
                    Console.WriteLine($" -Id: {user.Id} \n -Username: {user.Username} \n -Password: {user.Password} \n -FirstName: {user.FirstName} \n -LastName: {user.LastName} \n -Role: {user.Role} \n -Created date: {user.CreatedDate} \n -Creator id: {user.CreatorId} \n -Modified date: {user.ModifiedDate} \n -Modifier id: {user.ModifierId}");
                    Console.WriteLine("---------------------");
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("The user list is empty.");
                Console.ResetColor();
            }
        }

        public void CreateUser()
        {
            Console.WriteLine("Input data for the new User");
            Console.Write("Username: ");
            string username = Console.ReadLine();
            Console.Write("Password: ");
            string password = Console.ReadLine();
            Console.Write("First Name: ");
            string firstName = Console.ReadLine();
            Console.Write("Last Name: ");
            string lastName = Console.ReadLine();
            Console.WriteLine("Choose Role:");
            Console.WriteLine("1 - RegularUser");
            Console.WriteLine("2 - Admin");
            Console.Write("Role: ");
            int role;
            var userRole = new Role();
            bool validRole = Int32.TryParse(Console.ReadLine(), out role);

            if (!validRole || role <= 0 || role > 2)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid role. You van choose only 1 or 2.");
                Console.ResetColor();
                return;
            }
            else
            {
                if (role == 1)
                {
                    userRole = Role.RegularUser;
                }
                else if (role == 2)
                {
                    userRole = Role.Admin;
                }
            }
            AddUser(username, password, firstName, lastName, userRole, currentUser.Id);
        }

        public void EditUser()
        {
            Console.WriteLine("Input ID for the User you are editing.");
            Console.Write("Id: ");
            int id;
            bool validId = Int32.TryParse(Console.ReadLine(), out id);
            Console.Write("Username: ");
            string username = Console.ReadLine();
            Console.Write("Password: ");
            string password = Console.ReadLine();
            Console.Write("First Name: ");
            string firstName = Console.ReadLine();
            Console.Write("Last Name: ");
            string lastName = Console.ReadLine();

            if (!validId || id <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid id.");
                Console.ResetColor();
                return;
            }
            else
            {
                EditUser(id, username, password, firstName, lastName);
            }
        }
        public void DeleteUser()
        {
            Console.WriteLine("Input ID for the User you want to delete.");
            Console.Write("Id: ");
            int id;
            bool validId = Int32.TryParse(Console.ReadLine(), out id);

            if (!validId || id <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid id.");
                Console.ResetColor();
                return;
            }
            else
            {
                RemoveUser(id);
            }
        }

        public void AddUser(string username, string password, string firstName, string lastName, Role role, int? creatorId)
        {
            User newUser = new User(username, password, firstName, lastName, role, creatorId);
            var existingUsernames = _dbContext.Users.Select(u => u.Username);
            var existingId = _dbContext.Users.Select(u => u.Id);

            if (_dbContext.Users.Contains(newUser))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User already exists.");
                Console.ResetColor();
                return;
            }
            else if (existingUsernames.Contains(newUser.Username))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Username already exists.");
                Console.ResetColor();
                return;
            }
           
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("User was created.");
            Console.ResetColor();

            _dbContext.Users.Add(newUser);
            _dbContext.SaveChanges();
        }

        public void EditUser(int id, string username, string password, string firstName, string lastName)
        {
            User editingUser = _dbContext.Users.Find(id);
            if (editingUser == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User does not exist.");
                Console.ResetColor();
                return;
            }
            else if (username != editingUser.Username)
            {
                if (_dbContext.Users.Any(u => u.Username == username && u.Id != editingUser.Id))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Username is already existing.");
                    Console.ResetColor();
                    return;
                }
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Editing was unsuccessful.");
                Console.ResetColor();
                return;
            }
            else
            {
                editingUser.Username = username;
                editingUser.Password = password;
                editingUser.FirstName = firstName;
                editingUser.LastName = lastName;
                editingUser.ModifiedDate = DateTime.Now;
                editingUser.ModifierId = currentUser.Id;
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("User was successful edited.");
            Console.ResetColor();

            _dbContext.Users.Update(editingUser);
            _dbContext.SaveChanges();
        }

        public void EditProfile()
        {
            Console.WriteLine("Input your new username:");
            string username = Console.ReadLine();
            Console.WriteLine("Input your new password:");
            string password = Console.ReadLine();
            Console.WriteLine("Input your new first name:");
            string firstName = Console.ReadLine();
            Console.WriteLine("Input your new last name:");
            string lastName = Console.ReadLine();

            if (username.Length < 2 || password.Length < 2 || firstName.Length < 2 || lastName.Length < 2)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("You can't have empty input or less then 2 characters!");
                Console.ResetColor();
                return;
            }
            else
            {
                currentUser.FirstName = firstName;
                currentUser.LastName = lastName;
                currentUser.Username = username;
                currentUser.Password = password;

                Console.ForegroundColor = ConsoleColor.DarkBlue;
                Console.WriteLine("Your profile has been updated.");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine($"Your new profile is:\n - Username: {currentUser.Username}\n - Password: {currentUser.Password} \n - First name: {currentUser.FirstName}\n - Last name: {currentUser.LastName}");
                Console.ResetColor();
            }

            _dbContext.Users.Update(currentUser);
            _dbContext.SaveChanges();
        }

        public void RemoveUser(int id)
        {
            User removeUser = _dbContext.Users.Find(id);

            if (removeUser == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User doesn't exist.");
                Console.ResetColor();
                return;
            }
            else if (removeUser.Id == currentUser.Id)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("You can not delete the Creator.");
                Console.ResetColor();
                return;
            }
            else
            {
                _dbContext.Users.Remove(removeUser);
                _dbContext.SaveChanges();

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User is removed!");
                Console.ResetColor();
            }
        }
    }
}
