﻿using Data;
using Microsoft.EntityFrameworkCore;
using Models.Entities;
using System;
using System.Linq;

namespace Services
{
    public class TaskService
    {
        private readonly ToDoDBContext _dbContext;

        public TaskService(ToDoDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void PrintTasks()
        {
            Console.WriteLine("Enter list id:");
            int listid;
            var isInputValid = Int32.TryParse(Console.ReadLine(), out listid);
            if (!isInputValid || listid <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid list id.");
                Console.ResetColor();
                return;
            }

            GetTaskByListId(listid);
            return;
        }

        public bool GetTaskByListId(int listId)
        {
            int currentUserId = UserService.currentUser.Id;
            var list = _dbContext.ToDoLists.Include(l => l.Users).SingleOrDefault(l => l.Id == listId);

            if (list == null || !list.Users.Any(u => u.Id == currentUserId))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"No accesible list for printing");
                Console.ResetColor();

                return false;
            }

            var accessibleTasks = _dbContext.TaskToDos.Where(l => l.ListId == listId && l.Users.Any(t => t.Id == currentUserId)).ToList();

            if (accessibleTasks.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"No available tasks in this list");
                Console.ResetColor();

                return false;
            }

            accessibleTasks.ForEach((TaskToDo task) => Console.WriteLine($"{{Task id = {task.Id}, \n Title = {task.Title}, \n Description = {task.Description}, \n Completed = {task.IsComplete}, \n CretedDate = {task.CreatedDate}, \n CreatorId = {task.CreatorId}, \n ModifiedId = {task.ModifierId}, \n ModifiedDate =  {task.ModifiedDate}}}"));
            Console.WriteLine("----------------");

            return true;
        }

        public void CreateTask()
        {
            int creatorId = UserService.currentUser.Id;
            Console.WriteLine("Input ID for the ToDo List.");
            Console.Write("Id: ");
            int listId;
            bool validListId = Int32.TryParse(Console.ReadLine(), out listId);
            Console.WriteLine("Input Task Title");
            Console.Write("Title: ");
            string taskTitle = Console.ReadLine();
            Console.Write("Description: ");
            string description = Console.ReadLine();
            if (!validListId || listId <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid list id.");
                Console.ResetColor();
                return;
            }
            else
            {
                CreateTask(listId, taskTitle, description);
            }
        }

        public void EditTask()
        {
            Console.WriteLine("Input ID for the Task you are editing.");
            Console.Write("Id: ");
            int id;
            bool validId = Int32.TryParse(Console.ReadLine(), out id);
            bool isComplete = false;
            if (!validId || id <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid id.");
                Console.ResetColor();
                return;
            }
            Console.Write("Title: ");
            string title = Console.ReadLine();
            Console.Write("Description: ");
            string description = Console.ReadLine();
            isComplete = true;

            EditTask(id, title, description, isComplete);
        }

        public void DeleteTask()
        {
            Console.WriteLine("Input ID for the Task you want to delete.");
            Console.Write("Id: ");
            int id;
            bool validId = Int32.TryParse(Console.ReadLine(), out id);

            if (!validId || id <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid id.");
                Console.ResetColor();
                return;
            }
            RemoveTask(id);
        }

        public void AssignTask()
        {
            Console.WriteLine("Input ID for the Task you want to assign.");
            Console.Write("Task Id: ");
            int taskId;
            bool validListId = Int32.TryParse(Console.ReadLine(), out taskId);
            Console.WriteLine("Input ID of the User you want to assign task.");
            Console.Write("User Id: ");
            int userId;
            bool validId = Int32.TryParse(Console.ReadLine(), out userId);

            if (!validListId || taskId <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid Task id.");
                Console.ResetColor();
                return;
            }
            if (!validId || userId <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid User id.");
                Console.ResetColor();
                return;
            }

            AssignTask(taskId, userId);
        }

        public void CompleteTask()
        {
            Console.WriteLine("Input ID for the Task you want to complete.");
            Console.Write("Task Id: ");
            int taskId;
            bool validListId = Int32.TryParse(Console.ReadLine(), out taskId);
            if (!validListId || taskId <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid Task id.");
                Console.ResetColor();
                return;
            }
            CompleteTask(taskId);
        }

        public bool CreateTask(int listId, string title, string description)
        {
            int currentUserId = UserService.currentUser.Id;
            var currentUser = _dbContext.Users.Find(currentUserId);
           
            if(!_dbContext.ToDoLists.Include(l => l.Users).Any(l => l.Id ==listId && l.Users.Any(u => u.Id == currentUserId)))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("List not accesible.");
                Console.ResetColor();
                return false;
            }

            TaskToDo newTask = new TaskToDo(listId, title, description, false, currentUserId);
            newTask.Users.Add(currentUser);

            _dbContext.TaskToDos.Add(newTask);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Task was created.");
            Console.ResetColor();

            _dbContext.SaveChanges();
            return true;
        }

        public bool EditTask(int id, string title, string description, bool isComplete)
        {
            int currentUserId = UserService.currentUser.Id;
            TaskToDo editTask = _dbContext.TaskToDos.Include(t => t.Users).SingleOrDefault(t => t.Id == id);
            if (editTask == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Task does not exist.");
                Console.ResetColor();
                return false;
            }
            else if (!editTask.Users.Any(u => u.Id == currentUserId))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User does not have permission to edit task.");
                Console.ResetColor();
                return false;
            }
            else
            {
                editTask.Title = title;
                editTask.Description = description;
                editTask.ModifierId = currentUserId;
                editTask.ModifiedDate = DateTime.Now;
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Task with id: {id} is edited.");
            Console.ResetColor();

            UserService.currentUser.ModifiedTasks.Add(editTask);
            _dbContext.Update(editTask);
            _dbContext.SaveChanges();
            return true;
        }

        public bool RemoveTask(int id)
        {
            int currentUserId = UserService.currentUser.Id;
            TaskToDo removedTask = _dbContext.TaskToDos.Include(t => t.Users).SingleOrDefault(t => t.Id == id);

            if (removedTask == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Task does not exist.");
                Console.ResetColor();
                return false;
            }
            else if (!removedTask.Users.Any(u => u.Id == currentUserId))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User doesn't have that task.");
                Console.ResetColor();
                return false;
            }
            else if (removedTask.CreatorId == currentUserId)
            {
                _dbContext.TaskToDos.Remove(removedTask);
            }
            else
            {
                removedTask.Users.Remove(UserService.currentUser);
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Task is removed.");
            Console.ResetColor();

            _dbContext.SaveChanges();
            return true;
        }

        public bool AssignTask(int taskId, int userId)
        {
            int currentUserId = UserService.currentUser.Id;
            TaskToDo assignedTask = _dbContext.TaskToDos.Include(t => t.Users).SingleOrDefault(t => t.Id == taskId);

            if (assignedTask == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Task doesn't exist or it is inaccessible.");
                Console.ResetColor();
                return false;
            }
            else if (!_dbContext.TaskToDos.Any(t => t.Id == taskId && t.Users.Any(t => t.Id == currentUserId)))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"User does not have access to the list.");
                Console.ResetColor();
                return false;
            }
            else if (assignedTask.Users.Any(u => u.Id == userId))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Task is already assigned to user.");
                Console.ResetColor();
                return false;
            }

            assignedTask.ModifierId = currentUserId;
            assignedTask.ModifiedDate = DateTime.Now;

            assignedTask.Users.Add(UserService.currentUser);
            UserService.currentUser.ModifiedTasks.Add(assignedTask);
            var user = _dbContext.Users.Find(userId);
            assignedTask.Users.Add(user);
            _dbContext.TaskToDos.Update(assignedTask);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Task: {taskId} is assigned to User with id: {userId}.");
            Console.ResetColor();
            _dbContext.SaveChanges();
            return true;
        }

        public bool CompleteTask(int taskId)
        {
            int currentUserId = UserService.currentUser.Id;
            TaskToDo task = _dbContext.TaskToDos.Include(t => t.Users).SingleOrDefault(t => t.Id == taskId);
            var taskExist = _dbContext.TaskToDos.Any(t => t.Id == taskId);

            if (!task.Users.Any(u => u.Id == currentUserId))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User is not authorised to complete this task.");
                Console.ResetColor();
                return false;
            }
            if (!taskExist)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("No such task.");
                Console.ResetColor();
                return false;
            }

            task.ModifierId = currentUserId;
            task.IsComplete = true;
            _dbContext.TaskToDos.Update(task);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Task: {taskId} is completed.");
            Console.ResetColor();
            _dbContext.SaveChanges();
            return true;
        }
    }
}
