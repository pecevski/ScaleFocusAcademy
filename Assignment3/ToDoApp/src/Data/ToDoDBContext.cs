﻿using Microsoft.EntityFrameworkCore;
using Models.Entities;

namespace Data
{
    public class ToDoDBContext : DbContext
    {
        private const string _connectionString = "Server=(LocalDb)\\MSSQLLocalDB;Database=ToDoAppDB;Trusted_Connection=True;";
        public ToDoDBContext()
        {

        }
        public ToDoDBContext(DbContextOptions options)
            : base(options)
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<ToDoList> ToDoLists { get; set; }
        public DbSet<TaskToDo> TaskToDos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // User Model
            modelBuilder
               .Entity<User>()
               .ToTable("Users")
               .HasKey(u => u.Id);

            modelBuilder
                .Entity<User>()
                .Property(u => u.Username)
                .HasMaxLength(20)
                .IsRequired();
            modelBuilder
                .Entity<User>()
                .HasIndex(u => u.Username)
                .IsUnique();

            modelBuilder
                .Entity<User>()
                .Property(u => u.Password)
                .HasMaxLength(20)
                .IsRequired();

            modelBuilder
               .Entity<User>()
               .Property(u => u.FirstName)
               .HasMaxLength(20)
               .IsRequired();

            modelBuilder
               .Entity<User>()
               .Property(u => u.LastName)
               .HasMaxLength(20)
               .IsRequired();

            modelBuilder
                .Entity<User>()
                .Property(u => u.Role)
                .IsRequired();

            modelBuilder
                .Entity<User>()
                .HasOne(u => u.Creator)
                .WithMany(u => u.CreatedUsers)
                .HasForeignKey(u => u.CreatorId);

            modelBuilder
                .Entity<User>()
                .Property(u => u.CreatedDate)
                .HasDefaultValueSql("GETDATE()");

            modelBuilder
                .Entity<User>()
                .HasOne(u => u.Modifier)
                .WithMany(u => u.ModifiedUsers)
                .HasForeignKey(u => u.ModifierId);

            modelBuilder
                .Entity<User>()
                .Property(u => u.ModifiedDate)
                .HasDefaultValueSql("GETDATE()");

            //ToDoList Model
            modelBuilder
                .Entity<ToDoList>()
                .ToTable("ToDoLists")
                .HasKey(l => l.Id);

            modelBuilder
                .Entity<ToDoList>()
                .Property(l => l.Title)
                .HasMaxLength(20)
                .IsRequired();
            modelBuilder
               .Entity<ToDoList>()
               .HasIndex(l => l.Title)
               .IsUnique();

            modelBuilder
               .Entity<ToDoList>()
               .HasOne(l => l.Creator)
               .WithMany(l => l.CreatedLists)
               .HasForeignKey(l => l.CreatorId);

            modelBuilder
                .Entity<ToDoList>()
                .Property(l => l.CreatedDate)
                .HasDefaultValueSql("GETDATE()")
                .IsRequired();

            modelBuilder
                .Entity<ToDoList>()
                .HasOne(l => l.Modifier)
                .WithMany(l => l.ModifiedLists)
                .HasForeignKey(l => l.ModifierId);

            modelBuilder
                .Entity<ToDoList>()
                .Property(l => l.ModifiedDate)
                .HasDefaultValueSql("GETDATE()");

            modelBuilder
                .Entity<ToDoList>()
                .HasMany(l => l.Users)
                .WithMany(l => l.ToDoLists);

            //TaskToDo Model
            modelBuilder
                .Entity<TaskToDo>()
                .ToTable("TaskToDos")
                .HasKey(l => l.Id);

            modelBuilder
                .Entity<TaskToDo>()
                .Property(t => t.Title)
                .HasMaxLength(20)
                .IsRequired();
            modelBuilder
               .Entity<TaskToDo>()
               .HasIndex(t => t.Title)
               .IsUnique();

            modelBuilder
               .Entity<TaskToDo>()
               .Property(t => t.Description)
               .HasMaxLength(150)
               .IsRequired();

            modelBuilder
               .Entity<TaskToDo>()
               .HasOne(t => t.Creator)
               .WithMany(t => t.CreatedTasks)
               .HasForeignKey(t => t.CreatorId);

            modelBuilder
                .Entity<TaskToDo>()
                .Property(t => t.CreatedDate)
                .HasDefaultValueSql("GETDATE()")
                .IsRequired();

            modelBuilder
                .Entity<TaskToDo>()
                .HasOne(t => t.Modifier)
                .WithMany(t => t.ModifiedTasks)
                .HasForeignKey(t => t.ModifierId);

            modelBuilder
                .Entity<TaskToDo>()
                .Property(t => t.ModifiedDate)
                .HasDefaultValueSql("GETDATE()");

            modelBuilder
                .Entity<TaskToDo>()
                .HasMany(t => t.Users)
                .WithMany(t => t.TaskToDos);

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
            optionsBuilder.UseLazyLoadingProxies();
            base.OnConfiguring(optionsBuilder);
        }
    }
}
