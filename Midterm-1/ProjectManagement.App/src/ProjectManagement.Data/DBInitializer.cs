﻿using ProjectManagement.Models.Entities;
using ProjectManagement.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Data
{
    public class DBInitializer
    {
        public void Initialize(PMDataBaseContext context)
        {
            if (context.Database.EnsureCreated())
            {
                SeedAdmin(context);
            }
        }

        public void SeedAdmin(PMDataBaseContext context)
        {
            User admin = new User()
            {
                Username = "admin",
                Password = "adminpass",
                FirstName = "admin",
                LastName = "admin",
                Role = Role.Admin,
            };

            context.Add(admin);
            context.SaveChanges();
        }
    }
}
