﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using ProjectManagement.Data;

namespace ProjectManagement.Data.Migrations
{
    [DbContext(typeof(PMDataBaseContext))]
    partial class PMDataBaseContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.6")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ProjectManagement.Models.Entities.Project", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("OwnerId")
                        .HasColumnType("int");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.HasKey("Id");

                    b.HasIndex("OwnerId");

                    b.ToTable("Projects");
                });

            modelBuilder.Entity("ProjectManagement.Models.Entities.ProjectTask", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("AssignedUserId")
                        .HasColumnType("int");

                    b.Property<int>("ProjectId")
                        .HasColumnType("int");

                    b.Property<int>("Status")
                        .HasColumnType("int");

                    b.Property<int>("TaskCreatorId")
                        .HasColumnType("int");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.HasKey("Id");

                    b.HasIndex("AssignedUserId");

                    b.HasIndex("ProjectId");

                    b.HasIndex("TaskCreatorId");

                    b.ToTable("ProjectTasks");
                });

            modelBuilder.Entity("ProjectManagement.Models.Entities.Team", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("ProjectId")
                        .HasColumnType("int");

                    b.Property<int>("TeamCreatorId")
                        .HasColumnType("int");

                    b.Property<string>("TeamName")
                        .IsRequired()
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.HasKey("Id");

                    b.HasIndex("ProjectId");

                    b.HasIndex("TeamCreatorId");

                    b.ToTable("Teams");
                });

            modelBuilder.Entity("ProjectManagement.Models.Entities.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.Property<int>("Role")
                        .HasColumnType("int");

                    b.Property<string>("Username")
                        .IsRequired()
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.HasKey("Id");

                    b.HasIndex("Username")
                        .IsUnique();

                    b.ToTable("Users");
                });

            modelBuilder.Entity("ProjectManagement.Models.Entities.WorkLog", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("TaskId")
                        .HasColumnType("int");

                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.Property<float>("WorkingHours")
                        .HasColumnType("real");

                    b.Property<DateTime>("WorkingPeriod")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("TaskId");

                    b.HasIndex("UserId");

                    b.ToTable("WorkLogs");
                });

            modelBuilder.Entity("TeamUser", b =>
                {
                    b.Property<int>("TeamMembersId")
                        .HasColumnType("int");

                    b.Property<int>("TeamsId")
                        .HasColumnType("int");

                    b.HasKey("TeamMembersId", "TeamsId");

                    b.HasIndex("TeamsId");

                    b.ToTable("UsersTeams");
                });

            modelBuilder.Entity("ProjectManagement.Models.Entities.Project", b =>
                {
                    b.HasOne("ProjectManagement.Models.Entities.User", "Owner")
                        .WithMany("CreatedProjects")
                        .HasForeignKey("OwnerId")
                        .OnDelete(DeleteBehavior.ClientCascade)
                        .IsRequired();

                    b.Navigation("Owner");
                });

            modelBuilder.Entity("ProjectManagement.Models.Entities.ProjectTask", b =>
                {
                    b.HasOne("ProjectManagement.Models.Entities.User", "AssignedUser")
                        .WithMany("ProjectTasks")
                        .HasForeignKey("AssignedUserId")
                        .OnDelete(DeleteBehavior.ClientCascade);

                    b.HasOne("ProjectManagement.Models.Entities.Project", "Project")
                        .WithMany("ProjectTasks")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.ClientCascade)
                        .IsRequired();

                    b.HasOne("ProjectManagement.Models.Entities.User", "TaskCreator")
                        .WithMany("CreatedTasks")
                        .HasForeignKey("TaskCreatorId")
                        .OnDelete(DeleteBehavior.ClientCascade)
                        .IsRequired();

                    b.Navigation("AssignedUser");

                    b.Navigation("Project");

                    b.Navigation("TaskCreator");
                });

            modelBuilder.Entity("ProjectManagement.Models.Entities.Team", b =>
                {
                    b.HasOne("ProjectManagement.Models.Entities.Project", "AssignedProject")
                        .WithMany("Teams")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.ClientCascade);

                    b.HasOne("ProjectManagement.Models.Entities.User", "TeamCreator")
                        .WithMany("CreatedTeams")
                        .HasForeignKey("TeamCreatorId")
                        .OnDelete(DeleteBehavior.ClientCascade)
                        .IsRequired();

                    b.Navigation("AssignedProject");

                    b.Navigation("TeamCreator");
                });

            modelBuilder.Entity("ProjectManagement.Models.Entities.WorkLog", b =>
                {
                    b.HasOne("ProjectManagement.Models.Entities.ProjectTask", "ProjectTask")
                        .WithMany("WorkLogs")
                        .HasForeignKey("TaskId")
                        .OnDelete(DeleteBehavior.ClientCascade)
                        .IsRequired();

                    b.HasOne("ProjectManagement.Models.Entities.User", "User")
                        .WithMany("WorkLogs")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.ClientCascade)
                        .IsRequired();

                    b.Navigation("ProjectTask");

                    b.Navigation("User");
                });

            modelBuilder.Entity("TeamUser", b =>
                {
                    b.HasOne("ProjectManagement.Models.Entities.User", null)
                        .WithMany()
                        .HasForeignKey("TeamMembersId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ProjectManagement.Models.Entities.Team", null)
                        .WithMany()
                        .HasForeignKey("TeamsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("ProjectManagement.Models.Entities.Project", b =>
                {
                    b.Navigation("ProjectTasks");

                    b.Navigation("Teams");
                });

            modelBuilder.Entity("ProjectManagement.Models.Entities.ProjectTask", b =>
                {
                    b.Navigation("WorkLogs");
                });

            modelBuilder.Entity("ProjectManagement.Models.Entities.User", b =>
                {
                    b.Navigation("CreatedProjects");

                    b.Navigation("CreatedTasks");

                    b.Navigation("CreatedTeams");

                    b.Navigation("ProjectTasks");

                    b.Navigation("WorkLogs");
                });
#pragma warning restore 612, 618
        }
    }
}
