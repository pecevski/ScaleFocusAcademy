﻿namespace ProjectManagement.Models.Entities
{
    public abstract class Entity  : IEntity
    {
        public int Id { get; set; }
    }
}
