﻿using System;

namespace ProjectManagement.Models.Entities
{
    public class WorkLog : Entity
    {
        public WorkLog()
        {
            WorkingPeriod = DateTime.UtcNow;
        }
        public DateTime WorkingPeriod { get; set; }
        public float WorkingHours { get; set; }
        public virtual ProjectTask ProjectTask { get; set; }
        public virtual User User { get; set; }
        public int UserId { get; set; }
        public int TaskId { get; set; }
    }
}
