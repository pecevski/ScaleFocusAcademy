﻿using ProjectManagement.Models.Enums;
using System;
using System.Collections.Generic;

namespace ProjectManagement.Models.Entities
{

    public class ProjectTask : Entity
    {
        public ProjectTask() : base()
        {
            WorkLogs = new List<WorkLog>();
        }
        public string Title { get; set; }
        public Status Status { get; set; }
        public virtual Project Project { get; set; }
        public virtual User TaskCreator { get; set; }
        public virtual User AssignedUser { get; set; }
        public virtual List<WorkLog> WorkLogs { get; set; }
        public int ProjectId { get; set; }
        public int TaskCreatorId { get; set; }
        public int? AssignedUserId { get; set; }
    }
}
