﻿using ProjectManagement.Models.Entities;
using System.Collections.Generic;

namespace ProjectManagement.Services.Contracts
{
    public interface IProjectManagement : IServices<Project>
    {
        IEnumerable<Project> GetAll();
        bool AssignTeamToProject(Team team, int projectId, User owner);
    }
}
