﻿using ProjectManagement.Models.Entities;
using System.Collections.Generic;

namespace ProjectManagement.Services.Contracts
{
    public interface IServices<T> where T : IEntity
    {
        T GetById(int id);
        bool Create(T entity);
        bool Update(int id, T entity);
        bool Delete(int id);
    }
}
