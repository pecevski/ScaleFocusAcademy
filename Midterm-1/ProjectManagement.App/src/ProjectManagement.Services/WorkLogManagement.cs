﻿using ProjectManagement.Data;
using ProjectManagement.Models.Entities;
using ProjectManagement.Services.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace ProjectManagement.Services
{
    public class WorkLogManagement : IWorkLogManagement
    {
        private readonly PMDataBaseContext _database;

        public WorkLogManagement(PMDataBaseContext database)
        {
            _database = database;
        }
        public IEnumerable<WorkLog> GetAll(int taskId)
        {
            return _database.WorkLogs.Where(w => w.TaskId == taskId).ToList();
        }
        public WorkLog GetById(int id)
        {
            return _database.WorkLogs.SingleOrDefault(w => w.Id == id);
        }
        public bool Create(WorkLog workLog)
        {
            _database.WorkLogs.Add(workLog);
            _database.SaveChanges();
            return true;
        }

        public bool Update(int id, WorkLog workLog)
        {
            WorkLog updateLog = _database.WorkLogs.Find(id);

            if(updateLog == null)
            {
                return false;
            }

            updateLog.WorkingPeriod = workLog.WorkingPeriod;
            updateLog.WorkingHours = workLog.WorkingHours;

            _database.WorkLogs.Update(updateLog);
            _database.SaveChanges();

            return true;
        }

        public bool Delete(int id)
        {
            WorkLog removeLog = _database.WorkLogs.Find(id);
            if(removeLog == null)
            {
                return false;
            }

            _database.WorkLogs.Remove(removeLog);
            _database.SaveChanges();
            return true;
        }
    }
}
