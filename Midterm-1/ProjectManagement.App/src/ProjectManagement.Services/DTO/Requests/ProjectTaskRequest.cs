﻿using ProjectManagement.Models.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectManagement.Services.DTO.Requests
{
    public class ProjectTaskRequest
    {
        [Required]
        [MinLength(2)]
        [MaxLength(20)]
        public string Title { get; set; }

        [Required]
        [Range(1, 2)]
        public Status Status { get; set; }
    }
}
