﻿using System.ComponentModel.DataAnnotations;

namespace ProjectManagement.Services.DTO.Requests
{
    public class ProjectRequest
    {
        [Required]
        [MinLength(2)]
        [MaxLength(20)]
        public string Title { get; set; }
    }
}
