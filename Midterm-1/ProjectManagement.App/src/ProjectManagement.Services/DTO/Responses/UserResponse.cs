﻿using ProjectManagement.Models.Enums;
using System.Collections.Generic;

namespace ProjectManagement.Services.DTO.Responses
{
    public class UserResponse
    {
        public UserResponse()
        {
            Teams = new List<TeamResponse>();
            CreatedProjects = new List<ProjectResponse>();
            ProjectTasks = new List<ProjectTaskResponse>();
            WorkLogs = new List<WorkLogResponse>();

        }
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Role Role { get; set; }
        public List<ProjectResponse> CreatedProjects { get; set; }
        public List<TeamResponse> Teams { get; set; }
        public List<ProjectTaskResponse> ProjectTasks { get; set; }
        public List<WorkLogResponse> WorkLogs { get; set; }
    }
}
