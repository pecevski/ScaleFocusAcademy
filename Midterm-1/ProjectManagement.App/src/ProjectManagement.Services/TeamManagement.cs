﻿using ProjectManagement.Data;
using ProjectManagement.Models.Entities;
using ProjectManagement.Services.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace ProjectManagement.Services
{
    public class TeamManagement : ITeamManagement
    {
        private readonly PMDataBaseContext _database;
        public TeamManagement(PMDataBaseContext database)
        {
            _database = database;
        }
        public IEnumerable<Team> GetAll()
        {
            return _database.Teams.ToList();
        }

        public Team GetById(int id)
        {
            return _database.Teams.SingleOrDefault(t => t.Id == id);
        }

        public bool Create(Team team)
        {
            if (_database.Teams.SingleOrDefault(t => t.TeamName == team.TeamName) != null)
            {
                return false;
            }

            _database.Teams.Add(team);
            _database.SaveChanges();

            return true;
        }

        public bool Update(int id, Team team)
        {
            Team updateTeam = _database.Teams.Find(id);

            if (updateTeam == null)
            {
                return false;
            }

            updateTeam.TeamName = team.TeamName;

            _database.Teams.Update(updateTeam);
            _database.SaveChanges();
            return true;
        }
        public bool Delete(int id)
        {
            Team removeTeam = _database.Teams.Find(id);

            if (removeTeam == null)
            {
                return false;
            }

            _database.Teams.Remove(removeTeam);
            _database.SaveChanges();
            return true;
        }

        public bool AssignUserToTeams(User user, int teamId)
        {
            Team team = _database.Teams.SingleOrDefault(t => t.Id == teamId);

            if (team == null || team.TeamMembers.Any(u => u.Id == user.Id))
            {
                return false;
            }

            team.TeamMembers.Add(user);
            _database.SaveChanges();
            return true;
        }

        public bool RemoveUserFromTeams(User user, int teamId)
        {
            Team team = _database.Teams.SingleOrDefault(t => t.Id == teamId);

            if (team == null || !team.TeamMembers.Any(u => u.Id == user.Id))
            {
                return false;
            }

            team.TeamMembers.Remove(user);
            _database.SaveChanges();
            return true;
        }
    }
}
