﻿using Microsoft.AspNetCore.Mvc;
using ProjectManagement.Models.Entities;
using ProjectManagement.Models.Enums;
using ProjectManagement.Services.Contracts;
using ProjectManagement.Services.DTO.Requests;
using ProjectManagement.Services.DTO.Responses;
using ProjectManagement.WebAPI.Authentication;
using ProjectManagement.WebAPI.Mappers;
using System.Collections.Generic;
using System.Linq;

namespace ProjectManagement.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WorkLogsController : ControllerBase
    {
        private readonly IWorkLogManagement _workLogService;
        private readonly ITaskManagement _taskService;
        private readonly IUserManagement _userService;

        public WorkLogsController(IWorkLogManagement workLogService, ITaskManagement taskService, IUserManagement userService)
        {
            _workLogService = workLogService;
            _taskService = taskService;
            _userService = userService;
        }


        [HttpGet]
        [Route("All")]
        public ActionResult<List<WorkLogResponse>> GetAll(int taskId)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            ProjectTask logFromTask = _taskService.GetById(taskId);

            if (logFromTask == null)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                List<WorkLogResponse> logs = new List<WorkLogResponse>();

                List<WorkLog> allLogs = _workLogService.GetAll(taskId).ToList();

                foreach (var log in allLogs)
                {
                    WorkLogResponse logResponse = WorkLogMapper.MapLog(log);
                    logs.Add(logResponse);
                }

                return logs;
            }

            return NoContent();
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<WorkLogResponse> Get(int id)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            WorkLog logsFromDB = _workLogService.GetById(id);

            if (logsFromDB == null)
            {
                return NotFound();
            }

            return WorkLogMapper.MapLog(logsFromDB);
        }

        [HttpPost]
        public IActionResult Create(int taskId, WorkLogRequest createLog)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            ProjectTask logsFromTask = _taskService.GetById(taskId);

            if (logsFromTask == null)
            {
                return NotFound();
            }

            WorkLog log = WorkLogMapper.MapWorkLogRequest(createLog);
            log.User = currentUser;
            log.ProjectTask = logsFromTask;

            bool isCreated = _workLogService.Create(log);

            if (isCreated && ModelState.IsValid)
            {

                return CreatedAtAction("Get", "WorkLogs", new { id = log.Id }, null);
            }

            return BadRequest();
        }

        [HttpPut]
        public IActionResult Update(WorkLogRequest logRequest, int logId)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            WorkLog logFromDB = _workLogService.GetById(logId);

            if (logFromDB == null)
            {
                return NotFound();
            }

            if (logFromDB.UserId != currentUser.Id)
            {
                return Unauthorized();
            }

            if (ModelState.IsValid)
            {
                WorkLog updateLog = WorkLogMapper.MapWorkLogRequest(logRequest);
                _workLogService.Update(logId, updateLog);
                return Ok("WorLog updated.");
            }

            return NoContent();
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            WorkLog logFromDB = _workLogService.GetById(id);

            if (logFromDB == null)
            {
                return NotFound();
            }

            if (logFromDB.UserId != currentUser.Id)
            {
                return Unauthorized();
            }

            if (_workLogService.Delete(id))
            {
                return Ok();
            }

            return NoContent();
        }
    }
}
