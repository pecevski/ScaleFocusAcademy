﻿using ProjectManagement.Models.Entities;
using ProjectManagement.Services.DTO.Requests;
using ProjectManagement.Services.DTO.Responses;

namespace ProjectManagement.WebAPI.Mappers
{
    public static class ProjectMapper
    {

        public static ProjectResponse MapProject(Project project)
        {
            var projectResponse = new ProjectResponse()
            {
                Id = project.Id,
                Title = project.Title,
            };
            return projectResponse;
        }

        public static Project MapProjectRequest(ProjectRequest projectRequest)
        {
            var project = new Project()
            {
                Title = projectRequest.Title,
            };
            return project;
        }
    }
}
