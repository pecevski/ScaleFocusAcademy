﻿using ProjectManagement.Models.Entities;
using ProjectManagement.Services.DTO.Requests;
using ProjectManagement.Services.DTO.Responses;

namespace ProjectManagement.WebAPI.Mappers
{
    public static class TaskMapper
    {
        public static ProjectTaskResponse MapTask(ProjectTask projectTask)
        {
            var taskResponse = new ProjectTaskResponse()
            {
                Id = projectTask.Id,
                Title = projectTask.Title,
                Status = projectTask.Status,
            };
            return taskResponse;
        }

        public static ProjectTask MapTaskRequest(ProjectTaskRequest taskRequest)
        {
            var projectTask = new ProjectTask()
            {
                Title = taskRequest.Title,
                Status = taskRequest.Status,
            };
            return projectTask;
        }
    }
}
