﻿using ProjectManagement.Models.Entities;
using ProjectManagement.Services.DTO.Requests;
using ProjectManagement.Services.DTO.Responses;

namespace ProjectManagement.WebAPI.Mappers
{
    public static class UserMapper
    {
        public static UserResponse MapUser(User user)
        {
            var userResponse = new UserResponse()
            {
                Id = user.Id,
                Username = user.Username,
                Password = user.Password,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Role = user.Role
            };
            return userResponse;
        }

        public static User MapUserRequest(UserRequest userRequest)
        {
            var user = new User()
            {
                Username = userRequest.Username,
                Password = userRequest.Password,
                FirstName = userRequest.FirstName,
                LastName = userRequest.LastName,
                Role = userRequest.Role
            };
            return user;
        }
    }
}
