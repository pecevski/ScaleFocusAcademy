﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMApp.Data.Enums
{
    public enum Status 
    {

        Pending = 1,
        Complete
    }
}
