﻿using System;

namespace PMApp.Data.Entities
{
    public interface IEntity
    {
        public string Id { get; set; }
    }
}
