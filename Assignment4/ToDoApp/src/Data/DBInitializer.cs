﻿using Models.Entities;
using Models.Enums;

namespace Data
{
    public class DBInitializer
    {
        public void Initialize(ToDoDBContext dbContext)
        {
            if (dbContext.Database.EnsureCreated())
            {
                SeedAdmin(dbContext);
            }
        }

        public void SeedAdmin(ToDoDBContext dbContext)
        {
            User admin = new User("admin", "adminpassword", "admin", "admin", Role.Admin, null);
           
            dbContext.Add(admin);
            dbContext.SaveChanges();
        }
    }
}
