﻿using System.ComponentModel.DataAnnotations;

namespace DTO.Models.Requests
{
    public class TaskToDoRequest
    {

        [Required]
        [MinLength(2)]
        [MaxLength(20)]
        public string Title { get; set; }

        [Required]
        [StringLength(150)]
        public string Description { get; set; }

        [Required]
        public bool IsComplete { get; set; }
    }
}
