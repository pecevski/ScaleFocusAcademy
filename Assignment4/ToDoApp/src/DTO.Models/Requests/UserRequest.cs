﻿using Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace DTO.Models.Requests
{
    public class UserRequest
    {
        [Required]
        [MinLength(2)]
        [MaxLength(20)]
        public string Username { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(20)]
        public string Password { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(20)]
        public string FirstName { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(20)]
        public string LastName { get; set; }

        [Required]
        [Range(1,2)]
        public Role Role { get; set; }

        [Required]
        public int? CreatorId;

        [Required]
        public int? ModifierId;
    }
}
