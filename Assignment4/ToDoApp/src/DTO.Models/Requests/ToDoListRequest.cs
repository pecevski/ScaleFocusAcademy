﻿using System.ComponentModel.DataAnnotations;

namespace DTO.Models.Requests
{
    public class ToDoListRequest
    {
        [Required]
        [MinLength(2)]
        [MaxLength(50)]
        public string Title { get; set; }
    }
}
