﻿using System.Collections.Generic;

namespace DTO.Models.Responses
{
    public class TaskToDoResponse : BaseResponse
    {
        public TaskToDoResponse()
        {
            Users = new List<UserResponse>();
        }
        public int ListId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsComplete { get; set; }
        public List<UserResponse> Users { get; set; }
    }
}
