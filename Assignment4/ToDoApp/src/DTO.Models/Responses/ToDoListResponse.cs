﻿using System.Collections.Generic;

namespace DTO.Models.Responses
{
    public class ToDoListResponse : BaseResponse
    {
        public ToDoListResponse()
        {
            Users = new List<UserResponse>();
        }
        public string Title { get; set; }
        public List<UserResponse> Users { get; set; }
    }
}
