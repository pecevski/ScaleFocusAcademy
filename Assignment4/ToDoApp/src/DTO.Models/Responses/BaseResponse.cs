﻿using System;

namespace DTO.Models.Responses
{
    public class BaseResponse
    {
        public int Id { get; set; }
        public int? CreatorId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifierId { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
