﻿using Models.Enums;
using System.Collections.Generic;

namespace DTO.Models.Responses
{
    public class UserResponse : BaseResponse
    {
        public UserResponse() 
        {
            ToDoLists = new List<ToDoListResponse>();
            TaskToDos = new List<TaskToDoResponse>();
        }
        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public Role Role { get; set; }
        public List<ToDoListResponse> ToDoLists { get; set; }
        public List<TaskToDoResponse> TaskToDos { get; set; }
    }
}
