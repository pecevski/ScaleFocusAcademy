﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Entities
{
    public class TaskToDo : Entity
    {
        public TaskToDo()
        {

        }
        public TaskToDo(int listId, string title, string description, bool isComplete, int creatorId) : base(creatorId)
        {
            this.ListId = listId;
            this.Title = title;
            this.Description = description;
            this.IsComplete = isComplete;
            Users = new List<User>();
        }

        public int ListId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsComplete { get; set; }
        public virtual List<User> Users { get; set; }

    }
}
