﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Entities
{
    public class Entity
    {
        public Entity()
        {

        }
        public Entity(int? creatorId)
        {
            CreatorId = creatorId;
            CreatedDate = DateTime.Now;
            ModifiedDate = DateTime.Now;
            ModifierId = creatorId;
        }

        public int Id { get; set; }
        public int? CreatorId { get; set; }
        public virtual User Creator { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifierId { get; set; }
        public virtual User Modifier { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
