﻿using Models.Enums;
using System;
using System.Collections.Generic;

namespace Models.Entities
{
    public class User : Entity
    {
        public User()
        {

        }
        public User(string username, string password, string firstName, string lastName, Role role, int? creatorId) : base(creatorId)
        {
            this.Username = username;
            this.Password = password;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Role = role;
        }
        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public Role Role;
        public virtual List<ToDoList> ToDoLists { get; set; }
        public virtual List<TaskToDo> TaskToDos { get; set; }
        public virtual ICollection<ToDoList> CreatedLists { get; set; }
        public virtual ICollection<ToDoList> ModifiedLists { get; set; }
        public virtual ICollection<TaskToDo> CreatedTasks { get; set; }
        public virtual ICollection<TaskToDo> ModifiedTasks { get; set; }
        public virtual ICollection<User> CreatedUsers { get; set; }
        public virtual ICollection<User> ModifiedUsers { get; set; }
    }
}