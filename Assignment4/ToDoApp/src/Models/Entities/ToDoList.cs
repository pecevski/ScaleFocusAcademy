﻿using System.Collections.Generic;

namespace Models.Entities
{
    public class ToDoList : Entity
    {
        public ToDoList()
        {

        }
        public ToDoList(string title, int creatorId) : base(creatorId)
        {
            this.Title = title;
            Users = new List<User>();
        }
        public string Title { get; set; }
        public virtual List<User> Users { get; set; }
    }
}
