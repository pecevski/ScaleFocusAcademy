﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoApp
{
    public class AppConfig
    {
        public string DefaultConnectionString { get; set; }
        public string ConnectionString { get; set; }
    }
}
