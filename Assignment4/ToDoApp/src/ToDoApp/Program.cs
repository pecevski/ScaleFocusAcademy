﻿using Data;
using Microsoft.Extensions.Configuration;
using Services;
using System;

namespace ToDoApp
{
    class Program
    {
        private static ToDoDBContext _dBContext;
        static void Main(string[] args)
        {
            //var config = InitOptions<AppConfig>();
            //_dBContext = new ToDoDBContext();

            //var connectionString = config.ConnectionString;
            //var dbInitializer = new DBInitializer();
            //dbInitializer.Initialize(_dBContext);

            //var userService = new UserService(_dBContext);

            //var toDoListService = new ToDoListService(_dBContext);

            //var taskService = new TaskService(_dBContext);

            //var menuService = new MenuService(userService, toDoListService, taskService);
            //menuService.MainMenu();

        }

        private static T InitOptions<T>() where T : new()
        {
            var config = InitConfig();
            return config.Get<T>();
        }

        private static IConfigurationRoot InitConfig()
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
                .AddJsonFile($"appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env}.json", true, true)
                .AddEnvironmentVariables();

            return builder.Build();
        }
    }
}
