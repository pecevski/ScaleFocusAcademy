﻿using Data;
using Microsoft.EntityFrameworkCore;
using Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services
{
    public class ToDoListService
    {
        private readonly ToDoDBContext _dbContext;

        public ToDoListService(ToDoDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<ToDoList> GetToDoList(int userId)
        {
            var currentUser = _dbContext.Users.SingleOrDefault(u => u.Id == userId);
            var listToDos = _dbContext.ToDoLists.Include(l => l.Users).Where(l => l.Users.Any(u => u.Id == currentUser.Id)).ToList();

            if (listToDos.Count > 0)
            {
                foreach (var list in listToDos)
                {
                    Console.WriteLine($"{{ id = {list.Id}, \n Title = {list.Title}, \n CretedDate = {list.CreatedDate}, \n CreatorId = {list.CreatorId}, \n ModifiedId = {list.ModifierId}, \n ModifiedDate =  {list.ModifiedDate}}}");
                    return listToDos;
                }
            }

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"User doesn't have access to list.");
            Console.ResetColor();

            return new List<ToDoList>();
        }

        public ToDoList GetListById(int listId)
        {
            return _dbContext.ToDoLists.SingleOrDefault(l => l.Id == listId);
        }

        public ToDoList GetListByTitle(string title)
        {
            return _dbContext.ToDoLists.SingleOrDefault(l => l.Title == title);
        }

        public bool AddNewList(string title, int creatorId)
        {
            var currentUser = _dbContext.Users.SingleOrDefault(u => u.Id == creatorId);

            ToDoList newList = new ToDoList(title, creatorId);
            newList.Users.Add(currentUser);

            _dbContext.ToDoLists.Add(newList);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("List was created.");
            Console.ResetColor();

            _dbContext.SaveChanges();
            return true;
        }

        public bool EditList(int id, string title, int creatorId)
        {
            var currentUser = _dbContext.Users.SingleOrDefault(u => u.Id == creatorId);
            ToDoList editList = _dbContext.ToDoLists.Include(l => l.Users).SingleOrDefault(l => l.Id == id);

            if (editList == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("List does not exist.");
                Console.ResetColor();
                return false;
            }
            else if (!editList.Users.Any(u => u.Id == currentUser.Id))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User does not have permission to edit list.");
                Console.ResetColor();
                return false;
            }

            editList.Title = title;
            editList.ModifiedDate = DateTime.Now;
            editList.ModifierId = currentUser.Id;

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"List with id: {id} is edited.");
            Console.ResetColor();

            _dbContext.ToDoLists.Update(editList);
            _dbContext.SaveChanges();
            return true;
        }

        public bool RemoveList(int id, int creatorId)
        {
            var currentUser = _dbContext.Users.SingleOrDefault(u => u.Id == creatorId);
            ToDoList removedList = _dbContext.ToDoLists.Include(l => l.Users).SingleOrDefault(l => l.Id == id);

            if (removedList == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("List does not exist.");
                Console.ResetColor();
                return false;
            }
            else if (!removedList.Users.Any(u => u.Id == currentUser.Id))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User does not have permission to delete list.");
                Console.ResetColor();
                return false;
            }
            else if (removedList.CreatorId == currentUser.Id)
            {
                _dbContext.ToDoLists.Remove(removedList);
            }

            removedList.Users.Remove(currentUser);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"List id: {id} is removed.");
            Console.ResetColor();

            _dbContext.SaveChanges();

            return true;
        }

        public bool ShareList(int listId, int userId, int creatorId)
        {
            var currentUser = _dbContext.Users.SingleOrDefault(u => u.Id == creatorId);
            ToDoList sharedList = _dbContext.ToDoLists.Include(l => l.Users).SingleOrDefault(l => l.Id == listId);
            bool userExists = _dbContext.Users.Any(u => u.Id == userId);

            if (sharedList == null || !userExists)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User or List does not exist.");
                Console.ResetColor();
                return false;
            }
            else if (!sharedList.Users.Any(u => u.Id == currentUser.Id))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User is unauthorised to share this list.");
                Console.ResetColor();
                return false;
            }
            else if (sharedList.Users.Any(u => u.Id == userId))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("List is already shared.");
                Console.ResetColor();
                return false;
            }

            sharedList.ModifierId = currentUser.Id;
            sharedList.ModifiedDate = DateTime.Now;
            var user = _dbContext.Users.Find(userId);
            sharedList.Users.Add(user);

            _dbContext.ToDoLists.Update(sharedList);
            _dbContext.SaveChanges();

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"List is shared to User with id: {userId}.");
            Console.ResetColor();
            return true;
        }
    }
}
