﻿using Data;
using Microsoft.EntityFrameworkCore;
using Models.Entities;
using Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services
{
    public class UserService
    {
        public static bool signed = false;

        public static User currentUser = new();

        private readonly ToDoDBContext _dbContext;

        public UserService(ToDoDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public User Login(string username, string password)
        {
            try
            {
                var user = _dbContext.Users.SingleOrDefault(u => u.Username == username && u.Password == password);
                if (user == null)
                {
                    signed = false;
                    return null;
                }

                if (user.Password == password)
                {
                    signed = true;
                    currentUser = user;
                    return user;
                }

                signed = false;
                return null;
            }
            catch (Exception)
            {
                signed = false;
                return null;
            }
        }

        public User GetUser(string userName, string password)
        {
            return _dbContext.Users.SingleOrDefault(u => u.Username == userName && u.Password == password);
        }

        public User GetUserById(int userId)
        {
 
            return _dbContext.Users.SingleOrDefault(u => u.Id == userId);
        }

        public List<User> ListOfUsers()
        {
            var users = _dbContext.Users.ToList();

            if (users.Count > 0)
            {
                foreach (var user in users)
                {
                    Console.WriteLine($" -Id: {user.Id} \n -Username: {user.Username} \n -Password: {user.Password} \n -FirstName: {user.FirstName} \n -LastName: {user.LastName} \n -Role: {user.Role} \n -Created date: {user.CreatedDate} \n -Creator id: {user.CreatorId} \n -Modified date: {user.ModifiedDate} \n -Modifier id: {user.ModifierId}");
                    Console.WriteLine("---------------------");
                    return users;
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("The user list is empty.");
                Console.ResetColor();
            }
            return new List<User>();
        }

        public bool AddUser(string username, string password, string firstName, string lastName, Role role, int? creatorId)
        {
            User newUser = new User(username, password, firstName, lastName, role, creatorId);
            var existingUsernames = _dbContext.Users.Select(u => u.Username);
            var existingId = _dbContext.Users.Select(u => u.Id);

            if (_dbContext.Users.Contains(newUser))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User already exists.");
                Console.ResetColor();
                return false;
            }
            else if (existingUsernames.Contains(newUser.Username))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Username already exists.");
                Console.ResetColor();
                return false;
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("User was created.");
            Console.ResetColor();

            _dbContext.Users.Add(newUser);
            _dbContext.SaveChanges();
            return true;
        }

        public bool EditUser(int id, string username, string password, string firstName, string lastName)
        {
            User editingUser = _dbContext.Users.Find(id);

            if (editingUser == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User does not exist.");
                Console.ResetColor();
                return false;
            }
            else if (username != editingUser.Username)
            {
                bool updatedUsernameExists = _dbContext.Users.Any(u => u.Username == username);
                if (updatedUsernameExists)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Username is already existing.");
                    Console.ResetColor();
                    return false;
                }
            }

            editingUser.Username = username;
            editingUser.Password = password;
            editingUser.FirstName = firstName;
            editingUser.LastName = lastName;
            editingUser.ModifiedDate = DateTime.Now;

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("User was successful edited.");
            Console.ResetColor();

            _dbContext.Users.Update(editingUser);
            _dbContext.SaveChanges();
            return true;
        }

        public bool RemoveUser(int id)
        {
            User removeUser = _dbContext.Users.Find(id);

            if (removeUser == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User doesn't exist.");
                Console.ResetColor();
                return false;
            }
            else if (removeUser.Id == currentUser.Id)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("You can not delete the Creator.");
                Console.ResetColor();
                return false;
            }

            _dbContext.Users.Remove(removeUser);
            _dbContext.SaveChanges();

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("User is removed!");
            Console.ResetColor();

            return true;
        }
    }
}
