﻿using Data;
using Microsoft.EntityFrameworkCore;
using Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services
{
    public class TaskService
    {
        private readonly ToDoDBContext _dbContext;

        public TaskService(ToDoDBContext dbContext)
        {
            _dbContext = dbContext;
        }


        public List<TaskToDo> GetTaskByListId(int listId, int userId)
        {
            var currentUser = _dbContext.Users.SingleOrDefault(u => u.Id == userId);
            var list = _dbContext.ToDoLists.Include(l => l.Users).SingleOrDefault(l => l.Id == listId);

            if (list == null || !list.Users.Any(u => u.Id == currentUser.Id))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"No accesible list for printing");
                Console.ResetColor();

                return null;
            }

            var accessibleTasks = _dbContext.TaskToDos.Where(l => l.ListId == listId && l.Users.Any(t => t.Id == currentUser.Id)).ToList();

            if (accessibleTasks.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"No available tasks in this list");
                Console.ResetColor();

                return null;
            }

            accessibleTasks.ForEach((TaskToDo task) => Console.WriteLine($"{{Task id = {task.Id}, \n Title = {task.Title}, \n Description = {task.Description}, \n Completed = {task.IsComplete}, \n CretedDate = {task.CreatedDate}, \n CreatorId = {task.CreatorId}, \n ModifiedId = {task.ModifierId}, \n ModifiedDate =  {task.ModifiedDate}}}"));
            Console.WriteLine("----------------");

            return accessibleTasks;
        }

        public TaskToDo GetTaskById(int id, int userId)
        {
            var currentUser = _dbContext.Users.SingleOrDefault(u => u.Id == userId);
            var accessibleTasks = _dbContext.TaskToDos.Where(t => t.Users.Any(t => t.Id == currentUser.Id)).ToList();

            if (accessibleTasks.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Unauthorized user.");
                Console.ResetColor();

                return null;
            }
            return _dbContext.TaskToDos.SingleOrDefault(t => t.Id == id);
        }

        public TaskToDo GetTaskByTitle(string title)
        {
            return _dbContext.TaskToDos.SingleOrDefault(t => t.Title == title);
        }

        public bool CreateTask(int listId, string title,int userId, string description)
        {
            var currentUser = _dbContext.Users.SingleOrDefault(u => u.Id == userId);
            var currentUserId = _dbContext.Users.Find(currentUser.Id);
           
            if(!_dbContext.ToDoLists.Include(l => l.Users).Any(l => l.Id ==listId && l.Users.Any(u => u.Id == currentUser.Id)))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("List not accesible.");
                Console.ResetColor();
                return false;
            }

            TaskToDo newTask = new TaskToDo(listId, title, description, false, currentUser.Id);
            newTask.Users.Add(currentUserId);

            _dbContext.TaskToDos.Add(newTask);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Task was created.");
            Console.ResetColor();

            _dbContext.SaveChanges();
            return true;
        }

        public bool EditTask(int id, string title, string description,int userId, bool isComplete)
        {
            var currentUser = _dbContext.Users.SingleOrDefault(u => u.Id == userId);
            TaskToDo editTask = _dbContext.TaskToDos.Include(t => t.Users).SingleOrDefault(t => t.Id == id);
            if (editTask == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Task does not exist.");
                Console.ResetColor();
                return false;
            }
            else if (!editTask.Users.Any(u => u.Id == currentUser.Id))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User does not have permission to edit task.");
                Console.ResetColor();
                return false;
            }
            else
            {
                editTask.Title = title;
                editTask.Description = description;
                editTask.ModifierId = currentUser.Id;
                editTask.ModifiedDate = DateTime.Now;
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Task with id: {id} is edited.");
            Console.ResetColor();

            _dbContext.Update(editTask);
            _dbContext.SaveChanges();
            return true;
        }

        public bool RemoveTask(int id, int userId)
        {
            var currentUser = _dbContext.Users.SingleOrDefault(u => u.Id == userId);
            TaskToDo removedTask = _dbContext.TaskToDos.Include(t => t.Users).SingleOrDefault(t => t.Id == id);

            if (removedTask == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Task does not exist.");
                Console.ResetColor();
                return false;
            }
            else if (!removedTask.Users.Any(u => u.Id == currentUser.Id))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User doesn't have that task.");
                Console.ResetColor();
                return false;
            }
            else if (removedTask.CreatorId == currentUser.Id)
            {
                _dbContext.TaskToDos.Remove(removedTask);
            }
           
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Task is removed.");
            Console.ResetColor();

            _dbContext.SaveChanges();
            return true;
        }

        public bool AssignTask(int taskId, int userId, int creatorId)
        {
            var currentUser = _dbContext.Users.SingleOrDefault(u => u.Id == creatorId);
            TaskToDo assignedTask = _dbContext.TaskToDos.Include(t => t.Users).SingleOrDefault(t => t.Id == taskId);

            if (assignedTask == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Task doesn't exist or it is inaccessible.");
                Console.ResetColor();
                return false;
            }
            else if (!_dbContext.TaskToDos.Any(t => t.Id == taskId && t.Users.Any(t => t.Id == currentUser.Id)))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"User does not have access to the list.");
                Console.ResetColor();
                return false;
            }
            else if (assignedTask.Users.Any(u => u.Id == userId))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Task is already assigned to user.");
                Console.ResetColor();
                return false;
            }

            assignedTask.ModifierId = currentUser.Id;
            assignedTask.ModifiedDate = DateTime.Now;

            var user = _dbContext.Users.Find(userId);
            assignedTask.Users.Add(user);
            _dbContext.TaskToDos.Update(assignedTask);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Task: {taskId} is assigned to User with id: {userId}.");
            Console.ResetColor();
            _dbContext.SaveChanges();
            return true;
        }

        public bool CompleteTask(int taskId, int userId)
        {
            var currentUser = _dbContext.Users.SingleOrDefault(u => u.Id == userId);
            TaskToDo task = _dbContext.TaskToDos.Include(t => t.Users).SingleOrDefault(t => t.Id == taskId);
            var taskExist = _dbContext.TaskToDos.Any(t => t.Id == taskId);

            if (!task.Users.Any(u => u.Id == currentUser.Id))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User is not authorised to complete this task.");
                Console.ResetColor();
                return false;
            }
            if (!taskExist)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("No such task.");
                Console.ResetColor();
                return false;
            }

            task.ModifierId = currentUser.Id;
            task.IsComplete = true;
            _dbContext.TaskToDos.Update(task);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Task: {taskId} is completed.");
            Console.ResetColor();
            _dbContext.SaveChanges();
            return true;
        }
    }
}
