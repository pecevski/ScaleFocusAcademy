﻿using DTO.Models.Requests;
using DTO.Models.Responses;
using Microsoft.AspNetCore.Mvc;
using Models.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDoApp.WebAPI.Authentication;

namespace ToDoApp.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ToDoListsController : ControllerBase
    {
        private readonly ToDoListService _toDoListService;
        private readonly UserService _userService;

        public ToDoListsController(ToDoListService toDoListService, UserService userService)
        {
            this._toDoListService = toDoListService;
            this._userService = userService;
        }

        [HttpGet]
        [Route("All")]
        public ActionResult<List<ToDoListResponse>> GetAll()
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return Unauthorized();
            }

            List<ToDoListResponse> toDoListsDTO = new List<ToDoListResponse>();

            foreach (var list in _toDoListService.GetToDoList(currentUser.Id))
            {
                toDoListsDTO.Add(new ToDoListResponse()
                {
                    Id = list.Id,
                    Title = list.Title,
                    CreatorId = list.CreatorId,
                    CreatedDate = list.CreatedDate,
                    ModifierId = list.ModifierId,
                    ModifiedDate = list.ModifiedDate
                });
            }
            return toDoListsDTO;
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<ToDoListResponse> Get(int id)
        {
            User currentUser = _userService.GetCurrentUser(Request);
            ToDoList toDoListFromDB = _toDoListService.GetListById(id);

            if(toDoListFromDB == null)
            {
                return NotFound();
            }

            if (!toDoListFromDB.Users.Any(u => u.Id == currentUser.Id))
            {
                return Unauthorized();
            }

            ToDoListResponse toDoLists = new ToDoListResponse()
            {
                Id = toDoListFromDB.Id,
                Title = toDoListFromDB.Title,
                CreatorId = toDoListFromDB.CreatorId,
                CreatedDate = toDoListFromDB.CreatedDate,
                ModifierId = toDoListFromDB.ModifierId,
                ModifiedDate = toDoListFromDB.ModifiedDate
            };

            return toDoLists;
        }

        [HttpPost]
        public IActionResult AddList(ToDoListRequest addList)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return Unauthorized();
            }

            bool isCreated = _toDoListService.AddNewList(addList.Title, currentUser.Id);

            if (isCreated && ModelState.IsValid)
            {
                ToDoList listFromDb = _toDoListService.GetListByTitle(addList.Title);

                return CreatedAtAction("Get", "ToDoLists", new { id = listFromDb.Id }, null);
            }

            return BadRequest();
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult Update(int id, ToDoListRequest list)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return Unauthorized();
            }

            bool listUpdateSucceed = false;

            if (ModelState.IsValid)
            {
                listUpdateSucceed = _toDoListService.EditList(id, list.Title, currentUser.Id);
            }

            if (listUpdateSucceed)
            {
                return Ok();
            }

            return NotFound();
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return Unauthorized();
            }

            if (_toDoListService.RemoveList(id, currentUser.Id))
            {
                return Ok();
            }

            return NotFound();
        }

        [HttpPut]
        [Route("Share")]
        public IActionResult Share(int listId, int assignedUserId)
        {
            User currentUser = _userService.GetCurrentUser(Request);
            User assignedUser = _userService.GetUserById(assignedUserId);

            if (currentUser == null)
            {
                return Unauthorized();
            }

            if (assignedUser == null)
            {
                return Unauthorized();
            }

            bool isShared = false;

            if (ModelState.IsValid)
            {
                isShared = _toDoListService.ShareList(listId, assignedUser.Id, currentUser.Id);
            }

            if (isShared)
            {
                return Ok();
            }

            return BadRequest();
        }
    }
}
