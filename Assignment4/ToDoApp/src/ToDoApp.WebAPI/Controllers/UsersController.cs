﻿using DTO.Models.Requests;
using DTO.Models.Responses;
using Microsoft.AspNetCore.Mvc;
using Models.Entities;
using Models.Enums;
using Services;
using System.Collections.Generic;
using ToDoApp.WebAPI.Authentication;

namespace ToDoApp.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService _userService;
        public UsersController(UserService userService)
        {
            _userService = userService;
        }
        [HttpGet]
        [Route("All")]
        public ActionResult<List<UserResponse>> GetAll()
        {
            User currentUser = _userService.GetCurrentUser(Request);
            if (currentUser == null)
            {
                return Unauthorized();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            if (ModelState.IsValid)
            {
                List<UserResponse> users = new List<UserResponse>();

                List<User> allUsers = _userService.ListOfUsers();
                foreach (var user in allUsers)
                {
                    users.Add(new UserResponse()
                    {
                        Id = user.Id,
                        Username = user.Username,
                        Password = user.Password,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Role = user.Role,
                        CreatorId = user.CreatorId,
                        CreatedDate = user.CreatedDate,
                        ModifierId = user.ModifierId,
                        ModifiedDate = user.ModifiedDate
                    });
                }

                return users;
            }

            return BadRequest();
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<UserResponse> Get(int id)
        {
            User currentUser = _userService.GetCurrentUser(Request);
            if (currentUser == null)
            {
                return Unauthorized();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            if (ModelState.IsValid)
            {
                var user = _userService.GetUserById(id);

                if(user == null)
                {
                    return NotFound();
                }
                return new UserResponse()
                {
                    Id = user.Id,
                    Username = user.Username,
                    Password = user.Password,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Role = user.Role,
                };
            }
            return BadRequest();
        }

        [HttpPost]
        public IActionResult Create(UserRequest user)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return Unauthorized();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            bool isCreated = _userService.AddUser(user.Username, user.Password, user.FirstName, user.LastName, user.Role, currentUser.Id);

            if (isCreated && ModelState.IsValid)
            {
                User userFromDB = _userService.GetUser(user.Username, user.Password);

                return CreatedAtAction("Get", "Users", new { id = userFromDB.Id }, null);
            }

            return BadRequest();
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult Update(int id, UserRequest user)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return Unauthorized();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            bool userUpdateSucceed = false;

            if (ModelState.IsValid)
            {
                userUpdateSucceed = _userService.EditUser(id, user.Username, user.Password, user.FirstName, user.LastName);
            }

            if (userUpdateSucceed)
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return Unauthorized();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            if (_userService.RemoveUser(id))
            {
                return Ok();
            }

            return NotFound();
        }
    }
}
