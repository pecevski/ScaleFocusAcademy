﻿using DTO.Models.Requests;
using DTO.Models.Responses;
using Microsoft.AspNetCore.Mvc;
using Models.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDoApp.WebAPI.Authentication;

namespace ToDoApp.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskToDosController : ControllerBase
    {
        private readonly TaskService _taskService;
        private readonly UserService _userService;
        private readonly ToDoListService _toDoListService;

        public TaskToDosController(TaskService tasktService, UserService userService, ToDoListService toDoListService)
        {
            this._taskService = tasktService;
            this._userService = userService;
            this._toDoListService = toDoListService;
        }

        [HttpGet]
        [Route("All/{listId}")]
        public ActionResult<List<TaskToDoResponse>> GetAll(int listId)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return Unauthorized();
            }

            ToDoList tasksFromDB = _toDoListService.GetListById(listId);

            if (tasksFromDB == null)
            {
                return NotFound();
            }

            List<TaskToDoResponse> taskToDos = new List<TaskToDoResponse>();

            foreach (var taskToDo in _taskService.GetTaskByListId(listId, currentUser.Id))
            {
                taskToDos.Add(new TaskToDoResponse()
                {
                    Id = taskToDo.Id,
                    ListId = taskToDo.ListId,
                    Title = taskToDo.Title,
                    Description = taskToDo.Description,
                    IsComplete = taskToDo.IsComplete,
                    CreatorId = taskToDo.CreatorId,
                    CreatedDate = taskToDo.CreatedDate,
                    ModifierId = taskToDo.ModifierId,
                    ModifiedDate = taskToDo.ModifiedDate
                });
            }
            return taskToDos;
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<TaskToDoResponse> Get(int id)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return Unauthorized();
            }

            TaskToDo tasksFromDB = _taskService.GetTaskById(id, currentUser.Id);

            if (tasksFromDB == null)
            {
                return NotFound();
            }

            TaskToDoResponse tasks = new TaskToDoResponse()
            {
                Id = tasksFromDB.Id,
                ListId = tasksFromDB.ListId,
                Title = tasksFromDB.Title,
                Description = tasksFromDB.Description,
                IsComplete = tasksFromDB.IsComplete,
                CreatorId = tasksFromDB.CreatorId,
                CreatedDate = tasksFromDB.CreatedDate,
                ModifierId = tasksFromDB.ModifierId,
                ModifiedDate = tasksFromDB.ModifiedDate
            };

            return tasks;
        }

        [HttpPost]
        public IActionResult AddTask(int listId, TaskToDoRequest addTask)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return Unauthorized();
            }

            ToDoList tasksFromDB = _toDoListService.GetListById(listId);

            if (tasksFromDB == null)
            {
                return NotFound();
            }

            bool isCreated = _taskService.CreateTask(tasksFromDB.Id, addTask.Title, currentUser.Id, addTask.Description);

            if (isCreated && ModelState.IsValid)
            {
                TaskToDo taskFromDb = _taskService.GetTaskByTitle(addTask.Title);

                return CreatedAtAction("Get", "TaskToDos", new { id = taskFromDb.Id }, null);
            }

            return BadRequest();
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult Update(int id, TaskToDoRequest task)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return Unauthorized();
            }

            TaskToDo tasksFromDB = _taskService.GetTaskById(id, currentUser.Id);

            if (tasksFromDB == null)
            {
                return NotFound();
            }

            bool taskUpdateSucceed = false;

            if (ModelState.IsValid)
            {
                taskUpdateSucceed = _taskService.EditTask(id, task.Title, task.Description, currentUser.Id, false);
            }

            if (taskUpdateSucceed)
            {
                return Ok();
            }
            return BadRequest();
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return Unauthorized();
            }

            TaskToDo tasksFromDB = _taskService.GetTaskById(id, currentUser.Id);

            if (tasksFromDB == null)
            {
                return NotFound();
            }

            if (!tasksFromDB.Users.Any(u => u.Id == currentUser.Id))
            {
                return Unauthorized();
            }

            if (_taskService.RemoveTask(id, currentUser.Id))
            {
                return Ok();
            }

            return BadRequest();
        }

        [HttpPut]
        [Route("Assign")]
        public IActionResult Assign(int taskId, int assignedUserId)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return Unauthorized();
            }

            User assignedUser = _userService.GetUserById(assignedUserId);

            if (assignedUser == null)
            {
                return NotFound();
            }

            TaskToDo tasksFromDB = _taskService.GetTaskById(taskId, currentUser.Id);

            if (tasksFromDB == null)
            {
                return NotFound();
            }

            if (!tasksFromDB.Users.Any(u => u.Id == currentUser.Id))
            {
                return Unauthorized();
            }

            bool isAssigned = false;

            if (ModelState.IsValid)
            {
                isAssigned = _taskService.AssignTask(taskId, assignedUser.Id, currentUser.Id);
            }

            if (isAssigned)
            {
                return Ok();
            }

            return BadRequest();
        }

        [HttpPut]
        [Route("CompleteTask/{taskId}")]
        public IActionResult CompleteTask(int taskId)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return Unauthorized();
            }

            TaskToDo tasksFromDB = _taskService.GetTaskById(taskId, currentUser.Id);

            if (tasksFromDB == null)
            {
                return NotFound();
            }

            bool isComplete = false;

            if (ModelState.IsValid)
            {
                isComplete = _taskService.CompleteTask(taskId, currentUser.Id);
            }

            if (isComplete)
            {
                return Ok();
            }

            return BadRequest();
        }
    }
}
