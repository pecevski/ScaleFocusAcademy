﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Models.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoApp.WebAPI.Authentication
{
    public static class Authentication
    {
        public static User GetCurrentUser(this UserService userService, HttpRequest request)
        {
            StringValues usernameHeader;
            StringValues passwordHeader;
            request.Headers.TryGetValue("Username", out usernameHeader);
            request.Headers.TryGetValue("Password", out passwordHeader);
            if (usernameHeader.Count != 0 && passwordHeader.Count != 0)
            {
                string username = usernameHeader.First();
                string password = passwordHeader.First();

                return  userService.GetUser(username, password);
            }

            return null;
        }

    }
}
