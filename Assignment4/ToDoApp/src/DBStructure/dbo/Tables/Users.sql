﻿CREATE TABLE [dbo].[Users] (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](20) NOT NULL,
	[Password] [nvarchar](20) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[Role] [int] NOT NULL,
	[CreatorId] [int] NULL,
	[CreatedDate] [DateTime] NULL,
	[ModifierId] [int] NULL,
	[ModifiedDate] [DateTime] NULL,
 CONSTRAINT [UC_Users] UNIQUE (Username),
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))