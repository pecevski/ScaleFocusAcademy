﻿CREATE TABLE [dbo].[ToDoLists] (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](20) NOT NULL,
	[CreatedDate] [DateTime] NOT NULL,
	[CreatorId] [int] FOREIGN KEY REFERENCES [Users](Id) NOT NULL,
	[ModifierId] [int] FOREIGN KEY REFERENCES [Users](Id) NULL,
	[ModifiedDate] [DateTime] NULL,
 CONSTRAINT [PK_ToDoLists] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))