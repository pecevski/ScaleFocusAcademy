﻿using AuthApp.Data.Entity;
using AuthApp.Models.DTO.Request;
using AuthApp.Models.DTO.Response;
using AuthApp.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthApp.WebApp.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Admin")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
            : base()
        {
            _userService = userService;
        }

        [HttpGet]
        [Route("All")]
        public async Task<List<UserResponse>> GetAll()
        {
            List<UserResponse> users = new List<UserResponse>();

            foreach (var user in await _userService.GetAll())
            {
                users.Add(new UserResponse()
                {
                    Id = user.Id,
                    UserName = user.UserName
                });
            }

            return users;
        }

        [HttpGet]
        [Route("{username}")]
        public async Task<UserResponse> Get(string username)
        {
            User userFromDB = await _userService.GetUserByUserName(username);
            return new UserResponse()
            {
                UserName = userFromDB.UserName,
            };
        }

        [HttpPost]
        public async Task<IActionResult> Post(UserRequest user)
        {

            bool result = await _userService.CreateUser(user.UserName, user.Password);

            if (result)
            {
                User userFromDB = await _userService.GetUserByUserName(user.UserName);

                return CreatedAtAction("Get", "Users", new { id = userFromDB.Id }, user);
            }

            return Unauthorized();
        }

    }
}
