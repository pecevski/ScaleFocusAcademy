﻿using AuthApp.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace AuthApp.WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class CurrentDateTimeController : ControllerBase
    {
        private readonly IDateTimeProvider _dateTimeProvider;

        public CurrentDateTimeController(IDateTimeProvider dateTimeProvider)
        {
            _dateTimeProvider = dateTimeProvider;
        }

        [HttpGet]
        public DateTime GetCurrentDate()
        {
            return DateTime.UtcNow;
        }
    }
}
