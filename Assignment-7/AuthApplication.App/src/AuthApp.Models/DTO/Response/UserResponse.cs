﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthApp.Models.DTO.Response
{
    public class UserResponse
    {

        public string Id { get; set; }

        public string UserName { get; set; }
    }
}
