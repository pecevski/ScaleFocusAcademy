﻿using AuthApp.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthApp.Services
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime dateTime = DateTime.UtcNow;
        public DateTime GetCurrentDate()
        {
            return dateTime;
        }
    }
}
