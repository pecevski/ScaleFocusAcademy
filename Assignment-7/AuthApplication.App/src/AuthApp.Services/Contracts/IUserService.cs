﻿using AuthApp.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace AuthApp.Services.Contracts
{
    public interface IUserService
    {
        public Task<IEnumerable<User>> GetAll();
        public Task<User> GetById(string id);
        public Task<User> GetUserByUserName(string username);
        Task<User> GetCurrentUser(ClaimsPrincipal principal);
        Task<bool> IsUserInRole(string userId, string roleName);
        public Task<bool> CreateUser(string userName, string password);
    }
}
