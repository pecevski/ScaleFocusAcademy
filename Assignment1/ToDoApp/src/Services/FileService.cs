﻿using Newtonsoft.Json;
using System.Collections;
using System.IO;

namespace Services
{
    public class FileService
    {
        private readonly string UsersFileName = "Users.json";
        public void WriteFile<T>( T data) where T : IEnumerable
        {
            string serializedData = JsonConvert.SerializeObject(data);

            File.WriteAllText(UsersFileName, serializedData);
        }

        public T ReadFile<T>() where T : IEnumerable, new()
        {
            if (File.Exists(UsersFileName))
            {
                string serializedData = File.ReadAllText(UsersFileName);
                return JsonConvert.DeserializeObject<T>(serializedData);
            }
            else
            {
                return new T();
            }
        }
    }
}
