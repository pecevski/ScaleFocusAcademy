﻿using Newtonsoft.Json;
using System.Collections;
using System.IO;

namespace Services
{
    public class ListFileService
    {
        private readonly string ListFileName = "ToDoList.json";
        public void WriteFile<T>(T data) where T : IEnumerable
        {
            string serializedData = JsonConvert.SerializeObject(data);

            File.WriteAllText(ListFileName, serializedData);
        }

        public T ReadFile<T>() where T : IEnumerable, new()
        {
            if (File.Exists(ListFileName))
            {
                string serializedData = File.ReadAllText(ListFileName);
                return JsonConvert.DeserializeObject<T>(serializedData);
            }
            else
            {
                return new T();
            }
        }
    }
}
