﻿using Models.Entities;
using Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services
{
    public class UserService
    {
        public static bool signed = false;

        public static List<User> userList;
        public static User currentUser;
        private FileService fileService;

        public UserService()
        {
            fileService = new FileService();
            userList = fileService.ReadFile<List<User>>();
            
            if(userList == null)
            {
                userList = new List<User>();
            }
        }

        public void SeedAdmin()
        {
            if (userList == null || userList.Count == 0)
            {
                AddUser(1, "admin", "adminpassword", "admin", "admin", Role.Admin, null);
            }
        }

        public User Login(string username, string password)
        {
            try
            {
                var user = userList.SingleOrDefault(u => u.Username == username && u.Password == password);
                if (user == null)
                {
                    signed = false;
                    return null;
                }

                if (user.Password == password)
                {
                    signed = true;
                    currentUser = user;
                    return user;
                }

                signed = false;
                return null;
            }
            catch (Exception)
            {
                signed = false;
                return null;
            }
        }

        public void ListOfUsers()
        {
            var users = userList;

            if (users.Count > 0)
            {
                foreach (var user in users)
                {
                    Console.WriteLine($" -Id: {user.Id} \n -Username: {user.Username} \n -Password: {user.Password} \n -FirstName: {user.FirstName} \n -LastName: {user.LastName} \n -Role: {user.Role} \n -Created date: {user.CreatedDate} \n -Creator id: {user.CreatorId} \n -Modified date: {user.ModifiedDate} \n -Modifier id: {user.ModifierId}");
                    Console.WriteLine("---------------------");
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("The user list is empty.");
                Console.ResetColor();
            }
        }

        public void CreateUser()
        {
            Console.WriteLine("Input data for the new User");
            Console.Write("Id: ");
            int id;
            bool validId = Int32.TryParse(Console.ReadLine(), out id);

            if (!validId || id <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Id can not be 0 or negative number.");
                Console.ResetColor();
                return;
            }
            else
            {
                Console.Write("Username: ");
                string username = Console.ReadLine();
                Console.Write("Password: ");
                string password = Console.ReadLine();
                Console.Write("First Name: ");
                string firstName = Console.ReadLine();
                Console.Write("Last Name: ");
                string lastName = Console.ReadLine();
                Console.WriteLine("Choose Role:");
                Console.WriteLine("1 - RegularUser");
                Console.WriteLine("2 - Admin");
                Console.Write("Role: ");
                int role;
                var userRole = new Role();
                bool validRole = Int32.TryParse(Console.ReadLine(), out role);
                if (!validRole || role <= 0 || role > 2)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Invalid role. You van choose only 1 or 2.");
                    Console.ResetColor();
                    return;
                }
                else
                {
                    if (role == 1)
                    {
                        userRole = Role.RegularUser;
                    }
                    else if (role == 2)
                    {
                        userRole = Role.Admin;
                    }
                }
                AddUser(Convert.ToInt32(id), username, password, firstName, lastName, userRole, currentUser.Id);
            }
        }

        public void EditUser()
        {
            Console.WriteLine("Input ID for the User you are editing.");
            Console.Write("Id: ");
            int id;
            bool validId = Int32.TryParse(Console.ReadLine(), out id);
            Console.Write("Username: ");
            string username = Console.ReadLine();
            Console.Write("Password: ");
            string password = Console.ReadLine();
            Console.Write("First Name: ");
            string firstName = Console.ReadLine();
            Console.Write("Last Name: ");
            string lastName = Console.ReadLine();

            if (!validId || id <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid id");
                Console.ResetColor();
                return;
            }
            else
            {
                EditUser(Convert.ToInt32(id), username, password, firstName, lastName);
            }
        }
        public void DeleteUser()
        {
            Console.WriteLine("Input ID for the User you want to delete.");
            Console.Write("Id: ");
            int id;
            bool validId = Int32.TryParse(Console.ReadLine(), out id);

            if (!validId || id <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid id");
                Console.ResetColor();
                return;
            }
            else
            {
                RemoveUser(id);
            }
        }

        public void AddUser(int id, string username, string password, string firstName, string lastName, Role role, int? creatorId)
        {
            User newUser = new User(id, username, password, firstName, lastName, role, creatorId);
            var existingUsername = userList.Select(u => u.Username);
            var existingId = userList.Select(u => u.Id);

            if (existingId.Contains(newUser.Id))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Id already exists.");
                Console.ResetColor();
            }
            else if (userList.Contains(newUser))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User already exists.");
                Console.ResetColor();
            }
            else if (existingUsername.Contains(newUser.Username))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Username already exists.");
                Console.ResetColor();
            }
            else
            {
                userList.Add(newUser);
                fileService.WriteFile<List<User>>(userList);
            }
        }

        public void EditUser(int id, string username, string password, string firstName, string lastName)
        {
            User editingUser = userList.Find(u => u.Id == id);
            if (editingUser == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User does not exist.");
                Console.ResetColor();
            }
            else if (username != editingUser.Username)
            {
                if (userList.Any(u => u.Username == username && u.Id != editingUser.Id))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Username is already existing");
                    Console.ResetColor();
                    return;
                }
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Editing was unsuccessful.");
                Console.ResetColor();
                return;
            }
            else
            {
                editingUser.Id = id;
                editingUser.Username = username;
                editingUser.Password = password;
                editingUser.FirstName = firstName;
                editingUser.LastName = lastName;
                editingUser.ModifiedDate = DateTime.Now;
                editingUser.ModifierId = currentUser.Id;
            }
            fileService.WriteFile<List<User>>(userList);
        }

        public void EditProfile()
        {
            Console.WriteLine("Input your new username:");
            string username = Console.ReadLine();
            Console.WriteLine("Input your new password:");
            string password = Console.ReadLine();
            Console.WriteLine("Input your new first name:");
            string firstName = Console.ReadLine();
            Console.WriteLine("Input your new last name:");
            string lastName = Console.ReadLine();

            currentUser.FirstName = firstName;
            currentUser.LastName = lastName;
            currentUser.Username = username;
            currentUser.Password = password;

            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine("Your profile has been updated.");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"Your new profile is:\n - Username: {currentUser.Username}\n - Password: {currentUser.Password} \n - First name: {currentUser.FirstName}\n - Last name: {currentUser.LastName}");
            Console.ResetColor();

            fileService.WriteFile<List<User>>(userList);
        }

        public void RemoveUser(int id)
        {
            User removeUser = userList.Find(u => u.Id == id);

            if (removeUser == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User doesn't exist.");
                Console.ResetColor();
            }
            else if (removeUser.Id == currentUser.Id)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("You can not delete the Creator.");
                Console.ResetColor();
            }
            else
            {
                userList.Remove(removeUser);
                fileService.WriteFile<List<User>>(userList);
            }
        }

    }
}
