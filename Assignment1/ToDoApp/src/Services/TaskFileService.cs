﻿using Newtonsoft.Json;
using System.Collections;
using System.IO;

namespace Services
{
    public class TaskFileService
    {
        private readonly string TaskFileName = "Task.json";
        public void WriteFile<T>(T data) where T : IEnumerable
        {
            string serializedData = JsonConvert.SerializeObject(data);

            File.WriteAllText(TaskFileName, serializedData);
        }

        public T ReadFile<T>() where T : IEnumerable, new()
        {
            if (File.Exists(TaskFileName))
            {
                string serializedData = File.ReadAllText(TaskFileName);
                return JsonConvert.DeserializeObject<T>(serializedData);
            }
            else
            {
                return new T();
            }
        }
    }
}
