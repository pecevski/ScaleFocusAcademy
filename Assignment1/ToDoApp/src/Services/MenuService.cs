﻿using Models.Enums;
using System;

namespace Services
{
    public class MenuService
    {
        private readonly UserService userService;
        private readonly ToDoListService toDoListService;
        private readonly TaskService taskService;
        public MenuService(UserService userService, ToDoListService toDoListService, TaskService taskService)
        {
            this.userService = userService;
            this.toDoListService = toDoListService;
            this.taskService = taskService;
        }
  
        public void MainMenu()
        {
            if (UserService.signed == false)
            {
                bool successfull = false;

                while (!successfull)
                {
                    Console.WriteLine("Select option: ");
                    Console.WriteLine("================");
                    Console.WriteLine("1 - LOGIN");
                    Console.WriteLine("2 - EXIT APP");
                    Console.WriteLine("================");
                    string chooseOption = Console.ReadLine();

                    switch (chooseOption)
                    {
                        case "1":
                            GetLoginInteraction();
                            break;
                        case "2":
                            GetExitInteraction();
                            break;
                        default:
                            PrintErrorResponse();
                            break;
                    }
                }
            }
        }

        public void GetLoginInteraction()
        {
            Console.WriteLine("Write your username:");
            var username = Console.ReadLine();

            Console.WriteLine("Enter your password:");
            var password = Console.ReadLine();

            var user = userService.Login(username, password);
            if (user == null)
            {
                UserService.signed = false;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Wrong username or password.");
                Console.ResetColor();
                MainMenu();
            }
            else
            {
                UserService.signed = true;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Welcome, {username}!");
                Console.ResetColor();
                Console.WriteLine("================");

                if (user.Role == Role.Admin)
                {
                    AdminViewOptions();
                }
                else
                {
                    ShowToDoListManagementView();
                }
            }
            Console.ReadLine();
        }

        public void GetLogoutInteraction()
        {
            Console.WriteLine("Would you like to logout? ");
            if (Console.ReadLine().ToLower() == "y" || Console.ReadLine().ToLower() == "yes")
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine(" You have been Logged out!");
                Console.ResetColor();
                Console.WriteLine("================");
            }
            Console.WriteLine("Click ENTER to continue");
            return;
        }

        public void GetExitInteraction()
        {
            Console.WriteLine("Input 'exit' if you want to end this app!");
            if (Console.ReadLine().ToLower() == "exit") Environment.Exit(0);
        }

        public void PrintErrorResponse()
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("You have made non available choice!");
            Console.ResetColor();
            Console.WriteLine("================");
            Console.ReadLine();
        }
        
        public void AdminViewOptions()
        {
            while(UserService.signed == true)
            {
                Console.WriteLine("Admin option: ");
                Console.WriteLine("================");
                Console.WriteLine("1 - Admin menu");
                Console.WriteLine("2 - ToDo List menu");
                Console.WriteLine("3 - Exit");
                Console.WriteLine("================");
                string adminOption = Console.ReadLine();

                switch (adminOption)
                {
                    case "1":
                        ShowUserManagementView();
                        break;
                    case "2":
                        ShowToDoListManagementView();
                        break;
                    case "3":
                        Environment.Exit(0);
                        break;
                    default:
                        PrintErrorResponse();
                        break;
                }
            }
        }

        public void ShowUserManagementView()
        {
            while (UserService.signed == true)
            {
                Console.WriteLine("Admin menu: ");
                Console.WriteLine("================");
                Console.WriteLine("1 - ListOfUsers");
                Console.WriteLine("2 - CreateUser");
                Console.WriteLine("3 - EditUser");
                Console.WriteLine("4 - DeleteUser");
                Console.WriteLine("5 - Logout");
                Console.WriteLine("6 - EXIT");
                Console.WriteLine("================");
                string adminOption = Console.ReadLine();
                
                switch (adminOption)
                {
                    case "1":
                        UserService.signed = true;
                        userService.ListOfUsers();
                        break;
                    case "2":
                        UserService.signed = true;
                        userService.CreateUser();
                        break;
                    case "3":
                        UserService.signed = true;
                        userService.EditUser();
                        break;
                    case "4":
                        UserService.signed = true;
                        userService.DeleteUser();
                        break;
                    case "5":
                        UserService.signed = false;
                        GetLogoutInteraction();
                        break;
                    case "6":
                        UserService.signed = false;
                        Environment.Exit(0);
                        break;
                    default:
                        break;
                }
            }
        }

        public void ShowToDoListManagementView()
        {
            while (UserService.signed == true)
            {
                Console.WriteLine("User option: ");
                Console.WriteLine("================");
                Console.WriteLine("1 - Edit Profile");
                Console.WriteLine("2 - Show ToDo Lists");
                Console.WriteLine("3 - Create List");
                Console.WriteLine("4 - Edit List");
                Console.WriteLine("5 - Delete List");
                Console.WriteLine("6 - Share List");
                Console.WriteLine("7 - Task Menu");
                Console.WriteLine("8 - Logout");
                Console.WriteLine("9 - EXIT");
                Console.WriteLine("================");
                string userOption = Console.ReadLine();
                switch (userOption)
                {
                    case "1":
                        UserService.signed = true;
                        userService.EditProfile();
                        break;
                    case "2":
                        UserService.signed = true;
                        toDoListService.GetToDoList();
                        break;
                    case "3":
                        UserService.signed = true;
                        toDoListService.CreateList();
                        break;
                    case "4":
                        UserService.signed = true;
                        toDoListService.EditList();
                        break;
                    case "5":
                        UserService.signed = true;
                        toDoListService.DeleteList();
                        break;
                    case "6":
                        UserService.signed = true;
                        toDoListService.ShareList();
                        break;
                    case "7":
                        UserService.signed = true;
                        ShowTaskManagementView();
                        break;
                    case "8":
                        UserService.signed = false;
                        GetLogoutInteraction();
                        break;
                    case "9":
                        UserService.signed = false;
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("No such option");
                        break;
                }
            }
        }

        public void ShowTaskManagementView()
        {
            while (UserService.signed == true)
            {
                Console.WriteLine("User option: ");
                Console.WriteLine("================");
                Console.WriteLine("1 - Print Tasks");
                Console.WriteLine("2 - Create Task");
                Console.WriteLine("3 - Edit Task");
                Console.WriteLine("4 - Delete Task");
                Console.WriteLine("5 - Assign Task");
                Console.WriteLine("6 - Complete Task");
                Console.WriteLine("7 - Logout");
                Console.WriteLine("8 - EXIT");
                Console.WriteLine("================");
                string userOption = Console.ReadLine();
                switch (userOption)
                {
                    
                    case "1":
                        UserService.signed = true;
                        taskService.PrintTasks();
                        break;
                    case "2":
                        UserService.signed = true;
                        taskService.CreateTask();
                        break;
                    case "3":
                        UserService.signed = true;
                        taskService.EditTask();
                        break;
                    case "4":
                        UserService.signed = true;
                        taskService.DeleteTask();
                        break;
                    case "5":
                        UserService.signed = true;
                        taskService.AssignTask();
                        break;
                    case "6":
                        UserService.signed = true;
                        taskService.CompleteTask();
                        break;
                    case "7":
                        UserService.signed = false;
                        GetLogoutInteraction();
                        break;
                    case "8":
                        UserService.signed = false;
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("No such option");
                        break;
                }
            }

        }
    }
}