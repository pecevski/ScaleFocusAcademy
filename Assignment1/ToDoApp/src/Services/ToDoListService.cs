﻿using Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services
{
    public class ToDoListService
    {
        public List<ToDoList> toDoList;
        private ListFileService listFileService;
        public ToDoListService()
        {
            listFileService = new ListFileService();
            toDoList = listFileService.ReadFile<List<ToDoList>>();

            if (toDoList == null)
            {
                toDoList = new List<ToDoList>();
            }
        }

        public void GetToDoList()
        {
            int currentUserId = UserService.currentUser.Id;
            var listToDos = toDoList.Where(t => t.CreatorId == currentUserId || t.UserAccessList.Contains(currentUserId)).ToList();

            if (listToDos.Count > 0)
            {
                listToDos.ForEach(list =>  Console.WriteLine($"{{ id = {list.Id}, Title = {list.Title}, CretedDate = {list.CreatedDate}, CreatorId = {list.CreatorId}, ModifiedId = {list.ModifierId}, ModifiedDate =  {list.ModifiedDate}}}"));
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"User with id: {currentUserId} doesn't have access to the ToDo list.");
                Console.ResetColor();
            }
        }

        public void CreateList()
        {
            int creatorId = UserService.currentUser.Id;

            Console.WriteLine("Input ID for the ToDo List.");
            Console.Write("Id: ");
            int listId;
            bool validId = Int32.TryParse(Console.ReadLine(), out listId);
            Console.WriteLine("Input Title for the ToDo List");
            Console.Write("Title: ");
            string listTitle = Console.ReadLine();

            if (!validId || listId <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid input.");
                Console.ResetColor();
                return;
            }
            else
            {
                AddNewList(listId, listTitle, creatorId);
            }
        }

        public void EditList()
        {
            Console.WriteLine("Input ID for the List you are editing.");
            Console.Write("Id: ");
            int id;
            bool validId = Int32.TryParse(Console.ReadLine(), out id);

            if (!validId || id <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid id.");
                Console.ResetColor();
                return;
            }
            else
            {
                Console.Write("Title: ");
                string title = Console.ReadLine();

                EditList(id, title);
            }
        }

        public void DeleteList()
        {
            Console.WriteLine("Input ID for the List you want to delete.");
            Console.Write("Id: ");
            int id;
            bool validId = Int32.TryParse(Console.ReadLine(), out id);

            if (!validId || id <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid id.");
                Console.ResetColor();
                return;
            }
            else
            {
                RemoveList(id);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"List id: {id} is removed.");
                Console.ResetColor();
            }
        }

        public void ShareList()
        {
            Console.WriteLine("Input ID for the List you want to share.");
            Console.Write("List Id: ");
            int listId;
            bool validListId = Int32.TryParse(Console.ReadLine(), out listId);
            Console.WriteLine("Input ID of the User you want to share the list.");
            Console.Write("User Id: ");
            int userId;
            bool validId = Int32.TryParse(Console.ReadLine(), out userId);

            if (!validListId || listId <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid List id.");
                Console.ResetColor();
                return;
            }
            if (!validId || userId <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid User id.");
                Console.ResetColor();
                return;
            }
           
                ShareList(listId, userId);
        }

        public bool AddNewList(int id, string title, int creatorId)
        {
            int currentUserId = UserService.currentUser.Id;
            var existingId = toDoList.Exists(t => t.Id == id);

            if (existingId == true)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("List already exists.");
                Console.ResetColor();
                return false;
            }
            else
            {
                ToDoList newList = new ToDoList(id, title, creatorId);
                toDoList.Add(newList);
                newList.UserAccessList.Add(currentUserId);
                listFileService.WriteFile<List<ToDoList>>(toDoList);
                return true;
            }
        }

        public bool EditList(int id, string title)
        {
            int currentUserId = UserService.currentUser.Id;
            ToDoList editList = toDoList.Find(t => t.Id == id);
            if (editList == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("List does not exist.");
                Console.ResetColor();
                return false;
            }
            else if (editList.CreatorId != currentUserId && !editList.UserAccessList.Contains(currentUserId))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User does not have permission to edit list.");
                Console.ResetColor();
                return false;
            }
            else
            {
                editList.Title = title;
                editList.ModifiedDate = DateTime.Now;
                editList.ModifierId = currentUserId;
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"List with id: {id} is edited.");
            Console.ResetColor();
            editList.UserAccessList.Add(currentUserId);
            listFileService.WriteFile<List<ToDoList>>(toDoList);
            return true;
        }

        public bool RemoveList(int id)
        {
            int currentUserId = UserService.currentUser.Id;
            ToDoList removeList = toDoList.Find(t => t.Id == id);

            if (removeList == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("List does not exist.");
                Console.ResetColor();
                return false;
            }
            if (removeList.CreatorId == currentUserId)
            {
                toDoList.Remove(removeList);
            }
            var isRemoved = removeList.UserAccessList.Remove(currentUserId);
            listFileService.WriteFile<List<ToDoList>>(toDoList);
            return isRemoved;
        }

        public bool ShareList(int listId, int userId)
        {
            int currentUserId = UserService.currentUser.Id;
            ToDoList shareList = toDoList.Find(l => l.Id == listId || l.CreatorId == currentUserId);
            bool userExists = UserService.userList.Exists(u => u.Id == userId);

            if (shareList == null || !userExists)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User or List does not exist.");
                Console.ResetColor();
                return false;
            }
            else if (!shareList.UserAccessList.Contains(currentUserId))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User is unauthorised to share this list.");
                Console.ResetColor();
                return false;
            }
            else if (shareList.UserAccessList.Contains(userId))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("List is already shared.");
                Console.ResetColor();
                return false;
            }

            shareList.ModifierId = UserService.currentUser.Id;
            shareList.ModifiedDate = DateTime.Now;
            shareList.UserAccessList.Add(userId);
            listFileService.WriteFile<List<ToDoList>>(toDoList);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"List is shared to User with id: {userId}.");
            Console.ResetColor();
            return true;
        }
    }
}
