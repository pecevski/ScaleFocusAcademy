﻿using Services;

namespace ToDoApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var userService = new UserService();
            userService.SeedAdmin();

            var toDoListService = new ToDoListService();
            var taskService = new TaskService();

            var menuService = new MenuService(userService, toDoListService, taskService);
            menuService.MainMenu();


        }
    }
}
