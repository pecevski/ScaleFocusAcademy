﻿using System;

namespace Models.Entities
{
    public class Entity
    {
        public Entity(int id, int? creatorId)
        {
            Id = id;
            CreatorId = creatorId;
            CreatedDate = DateTime.Now;
            ModifiedDate = DateTime.Now;
            ModifierId = creatorId;
        }

        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? CreatorId { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int? ModifierId { get; set; }
    }
}
