﻿using Models.Enums;

namespace Models.Entities
{
    public class User : Entity
    {
        public User(int id, string username, string password, string firstName, string lastName, Role role, int? creatorId) : base(id, creatorId)
        {
            this.Username = username;
            this.Password = password;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Role = role;
        }

        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public Role Role;
    }
}