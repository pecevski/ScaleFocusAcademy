﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace Data
{
    public class FileDB
    {
        public void Write<T>(string fileName, T data)
        {
            string serializedData = JsonConvert.SerializeObject(data);

            File.WriteAllText(fileName, serializedData);

        }

        public T Read<T>(string fileName) where T : class
        {
            if (File.Exists(fileName))
            {
                string serializedData = File.ReadAllText(fileName);
                return JsonConvert.DeserializeObject<T>(serializedData);
            }
            else
            {
                return null;
            }
        }
    }
}
