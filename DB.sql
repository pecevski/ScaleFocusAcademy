CREATE DATABASE ToDoAppDB
GO

USE [ToDoAppDB]

DROP TABLE IF EXISTS [dbo].[Users];
DROP TABLE IF EXISTS [dbo].[ToDoLists];
DROP TABLE IF EXISTS [dbo].[ToDoTasks];
GO

CREATE TABLE [dbo].[Users] (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Usersname] [nvarchar](20) NOT NULL,
	[Password] [nvarchar](20) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[CreatedDate] [DateTime] NOT NULL,
	[CreatorId] [int] NOT NULL,
	[Role] [int] NOT NULL,
	[ModifierId] [int] NULL,
	[ModifiedDate] [DateTime] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))
GO

CREATE TABLE [dbo].[ToDoLists] (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](20) NOT NULL,
	[CreatedDate] [DateTime] NOT NULL,
	[CreatorId] [int] FOREIGN KEY REFERENCES [Users](Id) NOT NULL,
	[ModifierId] [int] NULL,
	[ModifiedDate] [DateTime] NULL,
 CONSTRAINT [PK_ToDoLists] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))
GO

CREATE TABLE [dbo].[ToDoTasks] (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[isComplete] [bit] NOT NULL,
	[ListId] [int] FOREIGN KEY REFERENCES ToDoLists(Id) NOT NULL,
	[CreatedDate] [DateTime] NOT NULL,
	[CreatorId] [int] FOREIGN KEY REFERENCES [Users](Id) NOT NULL,
	[ModifierId] [int] NULL,
	[ModifiedDate] [DateTime] NULL,
 CONSTRAINT [PK_ToDoTasks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))
GO

CREATE TABLE [dbo].[ListUserAccess]
(
	[UserId] INT REFERENCES [Users](Id) NOT NULL,
	[ListId] INT REFERENCES ToDoLists(Id) NOT NULL,
)
GO

CREATE TABLE [dbo].[TaskUserAccess]
(
	[UserId] INT REFERENCES [Users](Id) NOT NULL,
	[TaskId] INT REFERENCES ToDoTasks(Id) NOT NULL,
)
GO