# Team Project

The Workforce Management API is a platform for tracking who is in and out of the office and general management of sick leaves, requests for vacations (paid and non-paid) and the respective approvals.The system orchestrates the workforce availability, tracking time offs, approvals and sick leaves.

Topics covered:

1. SCRUM
2. GIT
3. ASP.NET 
4. REST API
5. IdentityServer

**Technologies implemented:**

-   Microsoft.EntityFrameworkCore 5.0; 
-   Microsoft.EntityFrameworkCore.SqlServer; 
-   Microsoft.EntityFrameworkCore.Tools; 
-   Microsoft.EntityFrameworkCore.Proxies; 
-   Microsoft.EntityFrameworkCore.Design; 
-   ASP.NET Core Web Api (with .NET 5.0);
-   Microsoft.ASP.NET Core.Identity.UI;
-   Microsoft.ASP.NET Core.Identity.EntityFrameworkCore;
-   Microsoft.ASP.NET Core.Authentication.JwtBearer;
-   Microsoft.ASP.NET Core.Authentication.OpenIdConnect;
-   IdentityServer4;
-   Swashbuckle.AspNetCore (Swagger);
-   Microsoft.Extensions.Hosting.Abstractions;
-   Nager.Date;

**Unit Test Technologies:**
-   Microsoft.NET.Test.Sdk;
-   coverlet.collector;
-   coverlet.msbuild;
-   xunit;
-   xunit.runner.visualstudio;
-   Moq;




