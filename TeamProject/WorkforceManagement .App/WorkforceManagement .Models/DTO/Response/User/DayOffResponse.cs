﻿namespace WorkforceManagement.Models.DTO.Response.User
{
    public class DayOffResponse
    {
        public int Paid { get; set; }

        public int Unpaid { get; set; }

        public int SickLeave { get; set; }
    }
}
