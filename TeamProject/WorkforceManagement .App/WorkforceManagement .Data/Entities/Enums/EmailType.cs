﻿namespace WorkforceManagement.Data.Entities.Enums
{
    public enum EmailType
    {
        Default = 1,
        Approved = 2,
        Rejected = 3,
    }
}