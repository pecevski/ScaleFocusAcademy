﻿CREATE TABLE [dbo].[TaskUserAccess]
(
	[UserId] INT REFERENCES [Users](Id) NOT NULL,
	[TaskId] INT REFERENCES ToDoTasks(Id) NOT NULL,
)
