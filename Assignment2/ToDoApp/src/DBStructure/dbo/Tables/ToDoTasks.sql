﻿CREATE TABLE [dbo].[ToDoTasks] (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[isComplete] [bit] NOT NULL,
	[ListId] [int] FOREIGN KEY REFERENCES ToDoLists(Id) NOT NULL,
	[CreatedDate] [DateTime] NOT NULL,
	[CreatorId] [int] FOREIGN KEY REFERENCES [Users](Id) NOT NULL,
	[ModifierId] [int] FOREIGN KEY REFERENCES [Users](Id) NULL,
	[ModifiedDate] [DateTime] NULL,
 CONSTRAINT [PK_ToDoTasks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))