﻿CREATE TABLE [dbo].[ListUserAccess]
(
	[UserId] INT REFERENCES [Users](Id) NOT NULL,
	[ListId] INT REFERENCES ToDoLists(Id) NOT NULL,
)