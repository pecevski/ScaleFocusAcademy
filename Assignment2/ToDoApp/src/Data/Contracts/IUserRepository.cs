﻿using System.Collections.Generic;

namespace Data.Contracts
{
    public interface IUserRepository<T> : IFullRepository<T>
    {
        T GetUserName(string username);
    }
}
