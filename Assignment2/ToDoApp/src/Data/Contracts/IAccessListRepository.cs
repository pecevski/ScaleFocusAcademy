﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Contracts
{
    public interface IAccessListRepository<T> : IBasicRepository<T>
    {
        IEnumerable<T> GetByUserId(int id);
        IEnumerable<T> GetByAccessibleObjectId(int id);
        void Create(T accessList);
    }
}
