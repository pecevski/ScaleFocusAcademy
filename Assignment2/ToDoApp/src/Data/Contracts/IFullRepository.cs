﻿
using System.Collections.Generic;

namespace Data.Contracts
{
    public interface IFullRepository<T> : IBasicRepository<T>
    {
        IEnumerable<T> GetAll();
        int Create(T entity);
        void Update(T entity);
    }
}
