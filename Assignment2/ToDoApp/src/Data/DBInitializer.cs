﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class DBInitializer
    {

        public void Initialize(string defaultConnectionString, string connectionString)
        {
            CreateDatabase(defaultConnectionString);
            CreateUserTable(connectionString);
            CreateLIstTable(connectionString);
            CreateTaskTable(connectionString);
            CreateUserListAccessTable(connectionString);
            CreateUserTaskAccessTable(connectionString);
        }
        private void CreateDatabase(string defaultConnectionString)
        {
            SqlConnection myConn = new SqlConnection(defaultConnectionString);
            string str = "CREATE DATABASE ToDoAppDB";

            SqlCommand myCommand = new SqlCommand(str, myConn);
            try
            {
                myConn.Open();
                myCommand.ExecuteNonQuery();
                Console.WriteLine("DataBase is Created Successfully");
            }
            catch 
            {
                Console.WriteLine("DataBase creation failed");
            }
            finally
            {
                if (myConn.State == ConnectionState.Open)
                {
                    myConn.Close();
                }
            }
        }

        private void CreateUserTable(string connectionString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string table = @"CREATE TABLE [dbo].[Users] (
	                                [Id] [int] IDENTITY(1,1) NOT NULL,
	                                [Username] [nvarchar](20) NOT NULL,
	                                [Password] [nvarchar](20) NOT NULL,
	                                [FirstName] [nvarchar](100) NOT NULL,
	                                [LastName] [nvarchar](100) NOT NULL,
	                                [Role] [int] NOT NULL,
	                                [CreatorId] [int] NULL,
	                                [CreatedDate] [DateTime] NULL,
	                                [ModifierId] [int] NULL,
	                                [ModifiedDate] [DateTime] NULL,
                                 CONSTRAINT [UC_Users] UNIQUE (Username),
                                 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
                                (
	                                [Id] ASC
                                ))";
                SqlCommand cmd = new SqlCommand(table, connection);
                try
                {
                    connection.Open();

                    cmd.ExecuteNonQuery();
                }
                catch 
                {
                    Console.WriteLine("UserTable creation failed");
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }

            }
        }

        private void CreateLIstTable(string connectionString)
        {

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string table = @"CREATE TABLE [dbo].[ToDoLists] (
	                                [Id] [int] IDENTITY(1,1) NOT NULL,
	                                [Title] [nvarchar](20) NOT NULL,
	                                [CreatedDate] [DateTime] NOT NULL,
	                                [CreatorId] [int] FOREIGN KEY REFERENCES [Users](Id) NOT NULL,
	                                [ModifierId] [int] FOREIGN KEY REFERENCES [Users](Id) NULL,
	                                [ModifiedDate] [DateTime] NULL,
                                 CONSTRAINT [PK_ToDoLists] PRIMARY KEY CLUSTERED 
                                (
	                                [Id] ASC
                                ))";
                SqlCommand cmd = new SqlCommand(table, connection);
                try
                {
                    connection.Open();

                    cmd.ExecuteNonQuery();
                }
                catch 
                {
                    Console.WriteLine("ListTable creation failed");
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }

            }
        }

        private void CreateTaskTable(string connectionString)
        {

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string table = @"CREATE TABLE [dbo].[ToDoTasks] (
	                                [Id] [int] IDENTITY(1,1) NOT NULL,
	                                [Title] [nvarchar](20) NOT NULL,
	                                [Description] [nvarchar](max) NOT NULL,
	                                [isComplete] [bit] NOT NULL,
	                                [ListId] [int] FOREIGN KEY REFERENCES ToDoLists(Id) NOT NULL,
	                                [CreatedDate] [DateTime] NOT NULL,
	                                [CreatorId] [int] FOREIGN KEY REFERENCES [Users](Id) NOT NULL,
	                                [ModifierId] [int] FOREIGN KEY REFERENCES [Users](Id) NULL,
	                                [ModifiedDate] [DateTime] NULL,
                                 CONSTRAINT [PK_ToDoTasks] PRIMARY KEY CLUSTERED 
                                (
	                                [Id] ASC
                                ))";
                SqlCommand cmd = new SqlCommand(table, connection);
                try
                {
                    connection.Open();

                    cmd.ExecuteNonQuery();
                }
                catch
                {
                    Console.WriteLine("TaskTable creation failed");
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }

            }
        }

        private void CreateUserListAccessTable(string connectionString)
        {

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string table = @"CREATE TABLE [dbo].[ListUserAccess]
                                    (
	                                    [UserId] INT REFERENCES [Users](Id) NOT NULL,
	                                    [ListId] INT REFERENCES ToDoLists(Id) NOT NULL,
                                    )";
                SqlCommand cmd = new SqlCommand(table, connection);
                try
                {
                    connection.Open();

                    cmd.ExecuteNonQuery();
                }
                catch 
                {
                    Console.WriteLine("UserListAccessTable creation failed");
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }

            }
        }

        private void CreateUserTaskAccessTable(string connectionString)
        {

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string table = @"CREATE TABLE [dbo].[TaskUserAccess]
                                        (
	                                        [UserId] INT REFERENCES [Users](Id) NOT NULL,
	                                        [TaskId] INT REFERENCES ToDoTasks(Id) NOT NULL,
                                        )";
                    SqlCommand cmd = new SqlCommand(table, connection);
                try
                {
                    connection.Open();

                    cmd.ExecuteNonQuery();
                }
                catch 
                {
                    Console.WriteLine("UserTaskAccessTable creation failed");
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }

            }
        }
    }
}
