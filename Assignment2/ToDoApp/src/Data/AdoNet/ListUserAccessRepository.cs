﻿using Data.Contracts;
using Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.AdoNet
{
    public class ListUserAccessRepository : IAccessListRepository<ListUserAccess>
    {
        private readonly string _connectionString;
        public ListUserAccessRepository(string connectionString)
        {
            _connectionString = connectionString; 
        }
        public void Create(ListUserAccess entity)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Connection = connection;

                    cmd.CommandText =
                       $"INSERT INTO dbo.[ListUserAccess] (userId, listId)" +
                       $" VALUES(@userId,@listId)";
                    cmd.Parameters.AddWithValue("@userId", entity.UserId);
                    cmd.Parameters.AddWithValue("@listId", entity.ListId);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public IEnumerable<ListUserAccess> GetByAccessibleObjectId(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Connection = connection;
                    cmd.CommandText = "SELECT * FROM dbo.[ListUserAccess]" +
                                      "WHERE listId = @listId";
                    cmd.Parameters.AddWithValue("@listId", id);

                    SqlDataReader dataReader = cmd.ExecuteReader();

                    List<ListUserAccess> links = new List<ListUserAccess>();

                    while (dataReader.Read())
                    {
                        int userId = dataReader.GetInt32(0);
                        int listId = dataReader.GetInt32(1);


                        var link = new ListUserAccess(userId, listId);
                        links.Add(link);
                    }
                    connection.Close();
                    return links;
                }
            }
        }

        public IEnumerable<ListUserAccess> GetByUserId(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Connection = connection;
                    cmd.CommandText = "SELECT * FROM dbo.[ListUserAccess]" +
                                     "WHERE userId = @userId";
                    cmd.Parameters.AddWithValue("@userId", id);

                    SqlDataReader dataReader = cmd.ExecuteReader();

                    List<ListUserAccess> links = new List<ListUserAccess>();

                    while (dataReader.Read())
                    {
                        int userId = dataReader.GetInt32(0);
                        int listId = dataReader.GetInt32(1);


                        var link = new ListUserAccess(userId, listId);
                        links.Add(link);
                    }
                    connection.Close();
                    return links;
                }
            }
        }

        public void Remove(ListUserAccess entity)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = connection;

                    cmd.CommandText = "DELETE FROM  dbo.[ListUserAccess] WHERE listId = @listId AND userId = @userId";
                    cmd.Parameters.AddWithValue("@listId", entity.ListId);
                    cmd.Parameters.AddWithValue("@userId", entity.UserId);

                    cmd.ExecuteNonQuery();

                }
                connection.Close();
            }
        }
    }
}
