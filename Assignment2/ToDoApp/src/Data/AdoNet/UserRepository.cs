﻿using Data.Contracts;
using Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Data.AdoNet
{
    public class UserRepository : IUserRepository<User>
    {
        private readonly string _connectionString;
        private readonly IAccessListRepository<ListUserAccess> _accessListRepository;
        private readonly IAccessListRepository<TaskUserAccess> _accessTaskRepository;

        public UserRepository(string connectionString, IAccessListRepository<ListUserAccess> accessListRepository, IAccessListRepository<TaskUserAccess> accessTaskRepository)
        {
            _connectionString = connectionString;
            _accessListRepository = accessListRepository;
            _accessTaskRepository = accessTaskRepository;
        }
        public IEnumerable<User> GetAll()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand())
                {

                    command.Connection = connection;

                    command.CommandText = "SELECT * FROM dbo.[Users]";

                    SqlDataReader reader = command.ExecuteReader();

                    List<User> users = new List<User>();

                    while (reader.Read())
                    {
                        int id = reader.GetInt32(0);
                        string userName = reader.GetString(1);
                        string password = reader.GetString(2);
                        string firstName = reader.GetString(3);
                        string lastName = reader.GetString(4);
                        int role = reader.GetInt32(5);
                        int? creatorId = !reader.IsDBNull(6) ? reader.GetInt32(6) : default(int);
                        DateTime creationDate = reader.GetDateTime(7);
                        int? ModifierId = !reader.IsDBNull(8) ? reader.GetInt32(8) : default(int);
                        DateTime ModifiedDate = reader.GetDateTime(9);

                        User user = new User(userName, password, firstName, lastName, (Models.Enums.Role)role, creatorId);
                        user.Id = id;
                        var listData = _accessListRepository.GetByUserId(id);
                        var taskData = _accessTaskRepository.GetByUserId(id);
                        user.AccessibleLists = listData.Select(l => l.ListId).ToList();
                        user.AccessibleTasks = taskData.Select(l => l.TaskId).ToList();
                       
                        users.Add(user);
                    }
                    connection.Close();
                    return users;
                }
            }
        }

        public int Create(User entity)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = connection;

                    cmd.CommandText =
                        $"INSERT INTO dbo.[Users] (Username, Password, FirstName, LastName, Role, CreatorId, CreatedDate, ModifierId, ModifiedDate) output INSERTED.ID " +
                        $" VALUES(@userUsername, @userPassword, @userFirstName, @userLastName, @userRole, @userCreatorId, GETDATE(), @userModifierId, GETDATE())";
                    cmd.Parameters.AddWithValue("@userUsername", entity.Username);
                    cmd.Parameters.AddWithValue("@userPassword", entity.Password);
                    cmd.Parameters.AddWithValue("@userFirstName", entity.FirstName);
                    cmd.Parameters.AddWithValue("@userLastName", entity.LastName);
                    cmd.Parameters.AddWithValue("@userRole", entity.Role);
                    cmd.Parameters.AddWithValue("@userCreatorId", (object)entity.CreatorId ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@userCreatedDate", entity.CreatedDate);
                    cmd.Parameters.AddWithValue("@userModifierId", (object)entity.ModifierId ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@userModifiedDate", entity.ModifiedDate);

                    int newId = (int)cmd.ExecuteScalar();
                    connection.Close();
                    return newId;
                }
            }
        }

        public void Update(User entity)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText = "UPDATE dbo.[Users] " +
                        "SET Username = @userUsername, Password = @userPassword, FirstName = @userFirstName, LastName = @userLastName, CreatorId = @userCreatorId, CreatedDate =  GETDATE(), ModifierId = @userModifierId, Modifieddate =  GETDATE()" +
                        "WHERE Id = @id";
                    cmd.Parameters.AddWithValue("@id", entity.Id);
                    cmd.Parameters.AddWithValue("@userUsername", entity.Username);
                    cmd.Parameters.AddWithValue("@userPassword", entity.Password);
                    cmd.Parameters.AddWithValue("@userFirstName", entity.FirstName);
                    cmd.Parameters.AddWithValue("@userLastName", entity.LastName);
                    cmd.Parameters.AddWithValue("@userCreatorId", (object)entity.CreatorId ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@userCreatedDate", entity.CreatedDate);
                    cmd.Parameters.AddWithValue("@userModifierId", (object)entity.ModifierId ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@userModifiedDate", entity.ModifiedDate);

                    cmd.ExecuteNonQuery();
                }
                connection.Close();
            }
        }

        public void Remove(User entity)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = connection;

                    cmd.CommandText = "DELETE FROM dbo.[Users] WHERE Id = @id";
                    cmd.Parameters.AddWithValue("@id", entity.Id);

                    cmd.ExecuteNonQuery();
                }
                connection.Close();
            }
        }

        public User GetUserName(string username)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = connection;

                    SqlDataReader reader = cmd.ExecuteReader();

                    cmd.CommandText = "SELECT * FROM [Users] WHERE [Username] = @username";
                    cmd.Parameters.AddWithValue("@userUsername", username);

                    User user = null;
                    while (reader.Read())
                    {
                        int id = reader.GetInt32(0);
                        string password = reader.GetString(2);
                        string firstName = reader.GetString(3);
                        string lastName = reader.GetString(4);
                        int role = reader.GetInt32(5);
                        int creatorId = !reader.IsDBNull(6) ? reader.GetInt32(6) : default(int);
                        DateTime creationDate = reader.GetDateTime(7);

                        user = new User(username, password, firstName, lastName, (Models.Enums.Role)role, creatorId);
                    }
                    connection.Close();
                    return user;
                }
            }
        }
    }
}
