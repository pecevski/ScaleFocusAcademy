﻿using Data.Contracts;
using Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Data.AdoNet
{
    public class TaskToDoRepository : IFullRepository<TaskToDo>
    {

        private readonly string _connectionString;
        private readonly IAccessListRepository<TaskUserAccess> _accessTaskRepository;

        public TaskToDoRepository(string connectionString, IAccessListRepository<TaskUserAccess> accessTaskRepository)
        {
            _connectionString = connectionString;
            _accessTaskRepository = accessTaskRepository;
        }
        public IEnumerable<TaskToDo> GetAll()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;

                    command.CommandText = "SELECT * FROM dbo.[ToDoTasks]";

                    SqlDataReader dataReader = command.ExecuteReader();

                    List<TaskToDo> tasks = new List<TaskToDo>();

                    while (dataReader.Read())
                    {
                        int id = dataReader.GetInt32(0);
                        string title = dataReader.GetString(1);
                        string description = dataReader.GetString(2);
                        bool isComplete = dataReader.GetBoolean(3);
                        int listId = dataReader.GetInt32(4);
                        DateTime createdDate = dataReader.GetDateTime(5);
                        int creatorId = dataReader.GetInt32(6);
                        int modifierId = dataReader.GetInt32(7);
                        DateTime modifiedDate = dataReader.GetDateTime(8);

                        var task = new TaskToDo(listId, title, description, isComplete, creatorId);
                        task.Id = id;
                        var links = _accessTaskRepository.GetByAccessibleObjectId(id);
                        task.UserIdAccessTask = links.Select(l => l.UserId).ToList();
                        tasks.Add(task);
                    }
                    connection.Close();

                    return tasks;
                }
            }
        }

        public int Create(TaskToDo entity)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = connection;

                    cmd.CommandText =
                        $"INSERT INTO dbo.[ToDoTasks] (ListId, Title, Description, isComplete, CreatorId, CreatedDate, ModifierId, ModifiedDate) output INSERTED.ID " +
                        $" VALUES(@taskListId, @taskTitle, @taskDescription, @taskComplete, @taskCreatorId, GETDATE(), @taskModifierId, GETDATE())";
                    cmd.Parameters.AddWithValue("@taskListId", entity.ListId);
                    cmd.Parameters.AddWithValue("@taskTitle", entity.Title);
                    cmd.Parameters.AddWithValue("@taskDescription", entity.Description);
                    cmd.Parameters.AddWithValue("@taskComplete", entity.IsComplete);
                    cmd.Parameters.AddWithValue("@taskCreatorId", entity.CreatorId);
                    cmd.Parameters.AddWithValue("@taskCreatedDate", entity.CreatedDate);
                    cmd.Parameters.AddWithValue("@taskModifierId", entity.ModifierId);
                    cmd.Parameters.AddWithValue("@taskModifiedDate", entity.ModifiedDate);

                    int newId = (int)cmd.ExecuteScalar();
                    connection.Close();
                    return newId;
                }
            }
        }

        public void Update(TaskToDo entity)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText = "UPDATE dbo.[ToDoTasks] " +
                        "SET ListId = @taskListId, Title = @taskTitle, Description = @taskDescription,isComplete = @taskComplete, CreatorId = @taskCreatorId, CreatedDate =  GETDATE(), ModifierId = @taskModifierId, Modifieddate =  GETDATE()" +
                        "WHERE Id = @id";
                    cmd.Parameters.AddWithValue("@id", entity.Id);
                    cmd.Parameters.AddWithValue("@taskListId", entity.ListId);
                    cmd.Parameters.AddWithValue("@taskTitle", entity.Title);
                    cmd.Parameters.AddWithValue("@taskDescription", entity.Description);
                    cmd.Parameters.AddWithValue("@taskComplete", entity.IsComplete);
                    cmd.Parameters.AddWithValue("@taskCreatorId", entity.CreatorId);
                    cmd.Parameters.AddWithValue("@taskCreatedDate", entity.CreatedDate);
                    cmd.Parameters.AddWithValue("@taskModifierId", entity.ModifierId);
                    cmd.Parameters.AddWithValue("@taskModifiedDate", entity.ModifiedDate);

                    cmd.ExecuteNonQuery();

                }
                connection.Close();
            }
        }

        public void Remove(TaskToDo entity)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = connection;

                    cmd.CommandText = "DELETE FROM  dbo.[ToDoTasks] WHERE Id = @id";
                    cmd.Parameters.AddWithValue("@id", entity.Id);

                    cmd.ExecuteNonQuery();

                }
                connection.Close();
            }
        }
    }
}
