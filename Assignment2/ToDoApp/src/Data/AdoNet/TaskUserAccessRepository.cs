﻿using Data.Contracts;
using Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.AdoNet
{
    public class TaskUserAccessRepository : IAccessListRepository<TaskUserAccess>
    {
        private readonly string _connectionString;
        public TaskUserAccessRepository(string connectionString)
        {
            _connectionString = connectionString;
        }
        public void Create(TaskUserAccess entity)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Connection = connection;

                    cmd.CommandText =
                       $"INSERT INTO dbo.[TaskUserAccess] (userId, taskId)" +
                       $" VALUES(@userId,@taskId)";
                    cmd.Parameters.AddWithValue("@userId", entity.UserId);
                    cmd.Parameters.AddWithValue("@taskId", entity.TaskId);

                    cmd.ExecuteNonQuery();
                }
            }

        }

        public IEnumerable<TaskUserAccess> GetByAccessibleObjectId(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Connection = connection;
                    cmd.CommandText = "SELECT * FROM dbo.[TaskUserAccess]" +
                                      "WHERE taskId = @taskId";
                    cmd.Parameters.AddWithValue("@taskId", id);

                    SqlDataReader dataReader = cmd.ExecuteReader();

                    List<TaskUserAccess> links = new List<TaskUserAccess>();

                    while (dataReader.Read())
                    {
                        int userId = dataReader.GetInt32(0);
                        int taskId = dataReader.GetInt32(1);

                        var link = new TaskUserAccess(userId, taskId);
                        links.Add(link);
                    }
                    connection.Close();
                    return links;
                }
            }
        }

        public IEnumerable<TaskUserAccess> GetByUserId(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Connection = connection;
                    cmd.CommandText = "SELECT * FROM dbo.[TaskUserAccess]" +
                                     "WHERE userId = @userId";
                    cmd.Parameters.AddWithValue("@userId", id);

                    SqlDataReader dataReader = cmd.ExecuteReader();

                    List<TaskUserAccess> links = new List<TaskUserAccess>();

                    while (dataReader.Read())
                    {
                        int userId = dataReader.GetInt32(0);
                        int taskId = dataReader.GetInt32(1);

                        var link = new  TaskUserAccess(userId, taskId);
                        links.Add(link);
                    }
                    connection.Close();
                    return links;
                }
            }
        }

        public void Remove(TaskUserAccess entity)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = connection;

                    cmd.CommandText = "DELETE FROM  dbo.[TaskUserAccess] WHERE taskId = @taskId AND userId = @userId";
                    cmd.Parameters.AddWithValue("@taskId", entity.TaskId);
                    cmd.Parameters.AddWithValue("@userId", entity.UserId);

                    cmd.ExecuteNonQuery();

                }
                connection.Close();
            }
        }
    }
}
