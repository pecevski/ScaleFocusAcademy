﻿using Data.Contracts;
using Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Data.AdoNet
{
    public class ToDoListRepository : IFullRepository<ToDoList>
    {
        private readonly string _connectionString;
        private readonly IAccessListRepository<ListUserAccess> _accessListRepository;

        public ToDoListRepository(string connectionString, IAccessListRepository<ListUserAccess> accessListRepository)
        {
            _connectionString = connectionString;
            _accessListRepository = accessListRepository;
        }

        public IEnumerable<ToDoList> GetAll()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText = "SELECT * FROM dbo.[ToDoLists]";

                    SqlDataReader dataReader = cmd.ExecuteReader();

                    List<ToDoList> lists = new List<ToDoList>();

                    while (dataReader.Read())
                    {
                        int id = dataReader.GetInt32(0);
                        string title = dataReader.GetString(1);
                        DateTime createdDate = dataReader.GetDateTime(2);
                        int creatorId = dataReader.GetInt32(3);
                        int modifierId = dataReader.GetInt32(4);
                        DateTime modifiedDate = dataReader.GetDateTime(5);

                        var list = new ToDoList(title, creatorId);
                        list.Id = id;
                        var links = _accessListRepository.GetByAccessibleObjectId(id);
                        list.UserAccessList = links.Select(l => l.UserId).ToList();

                        lists.Add(list);
                    }
                    connection.Close();
                    return lists;
                }
            }
        }

        public int Create(ToDoList entity)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = connection;

                    cmd.CommandText =
                        $"INSERT INTO dbo.[ToDoLists] (Title, CreatorId, CreatedDate, ModifierId , ModifiedDate) output INSERTED.ID " +
                        $"VALUES(@listTitle,  @listCreatorId, GETDATE(), @listModifierId, GETDATE())";
                    cmd.Parameters.AddWithValue("@listTitle", entity.Title);
                    cmd.Parameters.AddWithValue("@listCreatorId", entity.CreatorId);
                    cmd.Parameters.AddWithValue("@listCreatedDate", entity.CreatedDate);
                    cmd.Parameters.AddWithValue("@listModifierId", entity.ModifierId);
                    cmd.Parameters.AddWithValue("@listModifiedDate", entity.ModifiedDate);

                    int newId = (int)cmd.ExecuteScalar();
                    connection.Close();
                    return newId;
                }
            }
        }

        public void Update(ToDoList entity)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText = "UPDATE dbo.[ToDoLists] " +
                        "SET Title = @listTitle, CreatorId = @listCreatorId, CreatedDate = GETDATE(), ModifierId = @listModifierId, ModifiedDate = GETDATE()" +
                        "WHERE Id = @id";
                    cmd.Parameters.AddWithValue("@id", entity.Id);
                    cmd.Parameters.AddWithValue("@listTitle", entity.Title);
                    cmd.Parameters.AddWithValue("@listCreatorId", entity.CreatorId);
                    cmd.Parameters.AddWithValue("@listCreatedDate", entity.CreatedDate);
                    cmd.Parameters.AddWithValue("@listModifierId", entity.ModifierId);
                    cmd.Parameters.AddWithValue("@listModifiedDate", entity.ModifiedDate);

                    cmd.ExecuteNonQuery();
                }
                connection.Close();
            }
        }

        public void Remove(ToDoList entity)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = connection;

                    cmd.CommandText = "DELETE FROM  dbo.[ToDoLists] WHERE Id = @id";
                    cmd.Parameters.AddWithValue("@id", entity.Id);

                    cmd.ExecuteNonQuery();
                }
                connection.Close();
            }
        }
    }
}
