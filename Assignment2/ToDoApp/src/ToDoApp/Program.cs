﻿using Data;
using Data.AdoNet;
using Microsoft.Extensions.Configuration;
using Models.Entities;
using Services;
using System;

namespace ToDoApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = InitOptions<AppConfig>();
            var defaultConnectionString = config.DefaultConnectionString;
            var connectionString = config.ConnectionString;
            var dbInitiializer = new DBInitializer();
            dbInitiializer.Initialize(defaultConnectionString, connectionString);

            var listUserAccessRepository = new ListUserAccessRepository(connectionString);
            var taskUserAccessRepository = new TaskUserAccessRepository(connectionString);

            var userRepository = new UserRepository(connectionString, listUserAccessRepository, taskUserAccessRepository);
            var userService = new UserService(userRepository, listUserAccessRepository, taskUserAccessRepository);
            userService.SeedAdmin();

            var toDoListRepository = new ToDoListRepository(connectionString, listUserAccessRepository);
            var toDoListService = new ToDoListService(toDoListRepository, listUserAccessRepository);

            var taskToDoRepository = new TaskToDoRepository(connectionString, taskUserAccessRepository);
            var taskService = new TaskService(taskToDoRepository, taskUserAccessRepository);

            var menuService = new MenuService(userService, toDoListService, taskService);
            menuService.MainMenu();

        }

        private static T InitOptions<T>() where T : new()
        {
            var config = InitConfig();
            return config.Get<T>();
        }

        private static IConfigurationRoot InitConfig()
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
                .AddJsonFile($"appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env}.json", true, true)
                .AddEnvironmentVariables();

            return builder.Build();
        }
    }
}
