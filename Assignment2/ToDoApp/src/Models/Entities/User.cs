﻿using Models.Enums;
using System.Collections.Generic;

namespace Models.Entities
{
    public class User : Entity
    {

        public User(string username, string password, string firstName, string lastName, Role role, int? creatorId) : base(creatorId)
        {
            this.Username = username;
            this.Password = password;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Role = role;
            AccessibleLists = new List<int>();
            AccessibleTasks = new List<int>();
        }

        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public Role Role;
        public List<int> AccessibleLists { get; set; }
        public List<int> AccessibleTasks { get; set; } 
    }
}