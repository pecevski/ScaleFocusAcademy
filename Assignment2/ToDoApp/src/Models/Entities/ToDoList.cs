﻿using System.Collections.Generic;

namespace Models.Entities
{
    public class ToDoList : Entity
    {
        public ToDoList(string title, int creatorId) : base(creatorId)
        {
            this.Title = title;
            UserAccessList = new List<int>();
        }

        public string Title { get; set; }
        public List<int> UserAccessList { get; set; }
    }
}
