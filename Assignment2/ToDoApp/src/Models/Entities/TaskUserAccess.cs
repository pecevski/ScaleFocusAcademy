﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Entities
{
    public class TaskUserAccess
    {
        public TaskUserAccess(int userId, int taskId)
        {
            UserId = userId;
            TaskId = taskId;
        }
        public int UserId{ get; set; }
        public int TaskId { get; set; }
    }
}
