﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Entities
{
    public class TaskToDo : Entity
    {
        public TaskToDo(int listId, string title, string description, bool isComplete, int creatorId) : base(creatorId)
        {
            this.ListId = listId;
            this.Title = title;
            this.Description = description;
            this.IsComplete = isComplete;
            this.UserIdAccessTask = new List<int>();
        }

        public int ListId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsComplete { get; set; }
        public List<int> UserIdAccessTask { get; set; }

    }
}
