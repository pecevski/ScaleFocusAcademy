﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Entities
{
    public class ListUserAccess
    {
        public ListUserAccess(int userId, int listId)
        {
            UserId = userId;
            ListId = listId;
        }
        public int UserId { get; set; }
        public int ListId { get; set; }
    }
}
