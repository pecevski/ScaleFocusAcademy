﻿using Data;
using Data.AdoNet;
using Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services
{
    public class ToDoListService
    {
        public static List<ToDoList> listCollection;

        private readonly ToDoListRepository _toDoListRepository;
        private readonly ListUserAccessRepository _listUserAccessRepository;
        public ToDoListService(ToDoListRepository toDoListRepository, ListUserAccessRepository listUserAccessRepository)
        {
            _toDoListRepository = toDoListRepository;
            _listUserAccessRepository = listUserAccessRepository;
            listCollection = _toDoListRepository.GetAll().ToList();

            if (listCollection == null)
            {
                listCollection = new List<ToDoList>();
            }
        }

        public void GetToDoList()
        {
            int currentUserId = UserService.currentUser.Id;
            var listToDos = listCollection.Where(t => t.CreatorId == currentUserId || t.UserAccessList.Contains(currentUserId)).ToList();

            if (listToDos.Count > 0)
            {
                listToDos.ForEach(list => Console.WriteLine($"{{ id = {list.Id}, Title = {list.Title}, CretedDate = {list.CreatedDate}, CreatorId = {list.CreatorId}, ModifiedId = {list.ModifierId}, ModifiedDate =  {list.ModifiedDate}}}"));
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"User doesn't have access to list.");
                Console.ResetColor();
            }
        }

        public void CreateList()
        {
            int creatorId = UserService.currentUser.Id;

            Console.WriteLine("Input Title for the ToDo List");
            Console.Write("Title: ");
            string listTitle = Console.ReadLine();

            AddNewList(listTitle, creatorId);
        }

        public void EditList()
        {
            Console.WriteLine("Input ID for the List you are editing.");
            Console.Write("Id: ");
            int id;
            bool validId = Int32.TryParse(Console.ReadLine(), out id);

            if (!validId || id <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid id.");
                Console.ResetColor();
                return;
            }
            else
            {
                Console.Write("Title: ");
                string title = Console.ReadLine();

                EditList(id, title);
            }
        }

        public void DeleteList()
        {
            Console.WriteLine("Input ID for the List you want to delete.");
            Console.Write("Id: ");
            int id;
            bool validId = Int32.TryParse(Console.ReadLine(), out id);

            if (!validId || id <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid id.");
                Console.ResetColor();
                return;
            }
            else
            {
                RemoveList(id);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"List id: {id} is removed.");
                Console.ResetColor();
            }
        }

        public void ShareList()
        {
            Console.WriteLine("Input ID for the List you want to share.");
            Console.Write("List Id: ");
            int listId;
            bool validListId = Int32.TryParse(Console.ReadLine(), out listId);
            Console.WriteLine("Input ID of the User you want to share the list.");
            Console.Write("User Id: ");
            int userId;
            bool validId = Int32.TryParse(Console.ReadLine(), out userId);

            if (!validListId || listId <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid List id.");
                Console.ResetColor();
                return;
            }
            if (!validId || userId <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid User id.");
                Console.ResetColor();
                return;
            }

            ShareList(listId, userId);
        }

        public bool AddNewList(string title, int creatorId)
        {
            int currentUserId = UserService.currentUser.Id;

            ToDoList newList = new ToDoList(title, creatorId);
            newList.UserAccessList.Add(currentUserId);
            UserService.currentUser.AccessibleLists.Add(newList.Id);
            newList.Id = _toDoListRepository.Create(newList);
            var listUserAccess = new ListUserAccess(currentUserId, newList.Id);
            _listUserAccessRepository.Create(listUserAccess);
            listCollection.Add(newList);

            return true;

        }

        public bool EditList(int id, string title)
        {
            int currentUserId = UserService.currentUser.Id;
            ToDoList editList = listCollection.Find(t => t.Id == id);
            if (editList == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("List does not exist.");
                Console.ResetColor();
                return false;
            }
            else if (!editList.UserAccessList.Contains(currentUserId))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User does not have permission to edit list.");
                Console.ResetColor();
                return false;
            }
            else
            {
                editList.Title = title;
                editList.ModifiedDate = DateTime.Now;
                editList.ModifierId = currentUserId;
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"List with id: {id} is edited.");
            Console.ResetColor();

            _toDoListRepository.Update(editList);

            return true;
        }

        public bool RemoveList(int id)
        {
            int currentUserId = UserService.currentUser.Id;
            ToDoList removedList = listCollection.Find(t => t.Id == id);

            if (removedList == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("List does not exist.");
                Console.ResetColor();
                return false;
            }

            var isRemoved = default(bool);
            if (removedList.CreatorId == currentUserId)
            {
                isRemoved = RemoveLinks(currentUserId, removedList, removedList.UserAccessList.ToArray());

                isRemoved = isRemoved && listCollection.Remove(removedList);
                _toDoListRepository.Remove(removedList);
            }
            else
            {
                isRemoved = RemoveLinks(currentUserId, removedList, currentUserId);
            }

            return isRemoved;
        }

        private bool RemoveLinks(int currentUserId, ToDoList removedList, params int[] linksToRemove)
        {
            var isRemoved = default(bool);
            foreach (var userId in linksToRemove)
            {
                var listUserAccess = new ListUserAccess(userId, removedList.Id);
                _listUserAccessRepository.Remove(listUserAccess);
            }

            isRemoved = removedList.UserAccessList.Remove(currentUserId);
            UserService.currentUser.AccessibleLists.Remove(removedList.Id);

            return isRemoved;
        }

        public bool ShareList(int listId, int userId)
        {
            int currentUserId = UserService.currentUser.Id;
            ToDoList shareList = listCollection.Find(l => l.Id == listId || l.CreatorId == currentUserId);
            bool userExists = UserService.userList.Any(u => u.Id == userId);

            if (shareList == null || !userExists)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User or List does not exist.");
                Console.ResetColor();
                return false;
            }
            else if (!shareList.UserAccessList.Contains(currentUserId))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User is unauthorised to share this list.");
                Console.ResetColor();
                return false;
            }
            else if (shareList.UserAccessList.Contains(userId))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("List is already shared.");
                Console.ResetColor();
                return false;
            }

            shareList.ModifierId = UserService.currentUser.Id;
            shareList.ModifiedDate = DateTime.Now;
            shareList.UserAccessList.Add(userId);
            var user = UserService.userList.Find(u => u.Id == userId);
            user.AccessibleLists.Add(listId);
            var listUserAccess = new ListUserAccess(userId, listId);

            _listUserAccessRepository.Create(listUserAccess);
            _toDoListRepository.Update(shareList);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"List is shared to User with id: {userId}.");
            Console.ResetColor();
            return true;
        }
    }
}
