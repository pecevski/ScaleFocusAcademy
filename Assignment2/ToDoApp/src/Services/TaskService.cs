﻿using Data;
using Data.AdoNet;
using Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services
{
    public class TaskService
    {
        private List<TaskToDo> taskList;

        private readonly TaskToDoRepository _taskToDorepository;
        private readonly TaskUserAccessRepository _taskUserAccessRepository;

        public TaskService(TaskToDoRepository taskToDoRepository, TaskUserAccessRepository taskUserAccessRepository)
        {
            _taskToDorepository = taskToDoRepository;
            _taskUserAccessRepository = taskUserAccessRepository;
            taskList = _taskToDorepository.GetAll().ToList();

            if (taskList == null)
            {
                taskList = new List<TaskToDo>();
            }
        }

        public void PrintTasks()
        {
            Console.WriteLine("Enter list id:");
            int listid;
            var isInputValid = Int32.TryParse(Console.ReadLine(), out listid);
            if (!isInputValid || listid <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid list id.");
                Console.ResetColor();
                return;
            }

            GetTaskByListId(listid);
            return;
        }

        public bool GetTaskByListId(int listId)
        {
            int currentUserId = UserService.currentUser.Id;
            if (!ToDoListService.listCollection.Any(l => l.Id == listId && l.UserAccessList.Contains(currentUserId)))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"No available list for printing");
                Console.ResetColor();

                return false;
            }

            var accessibleTasks = taskList.Where(l => l.ListId == listId && l.UserIdAccessTask.Contains(currentUserId)).ToList();
            if (accessibleTasks.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"No available tasks in this list");
                Console.ResetColor();

                return false;
            }

            accessibleTasks.ForEach((TaskToDo task) => Console.WriteLine($"{{Task id = {task.Id}, \n Title = {task.Title}, \n Description = {task.Description}, \n Completed = {task.IsComplete}, \n CretedDate = {task.CreatedDate}, \n CreatorId = {task.CreatorId}, \n ModifiedId = {task.ModifierId}, \n ModifiedDate =  {task.ModifiedDate}}}"));
            Console.WriteLine("----------------");

            return true;
        }

        public void CreateTask()
        {
            int creatorId = UserService.currentUser.Id;
            Console.WriteLine("Input ID for the ToDo List.");
            Console.Write("Id: ");
            int listId;
            bool validListId = Int32.TryParse(Console.ReadLine(), out listId);
            Console.WriteLine("Input Task Title");
            Console.Write("Title: ");
            string taskTitle = Console.ReadLine();
            Console.Write("Description: ");
            string description = Console.ReadLine();
            if (!validListId || listId <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid list id.");
                Console.ResetColor();
                return;
            }
            else
            {
                CreateTask(listId, taskTitle, description);
            }
        }

        public void EditTask()
        {
            Console.WriteLine("Input ID for the Task you are editing.");
            Console.Write("Id: ");
            int id;
            bool validId = Int32.TryParse(Console.ReadLine(), out id);
            bool isComplete = false;
            if (!validId || id <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid id.");
                Console.ResetColor();
                return;
            }
            Console.Write("Title: ");
            string title = Console.ReadLine();
            Console.Write("Description: ");
            string description = Console.ReadLine();
            isComplete = true;

            EditTask(id, title, description, isComplete);
        }

        public void DeleteTask()
        {
            Console.WriteLine("Input ID for the Task you want to delete.");
            Console.Write("Id: ");
            int id;
            bool validId = Int32.TryParse(Console.ReadLine(), out id);

            if (!validId || id <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid id.");
                Console.ResetColor();
                return;
            }
            RemoveTask(id);
        }

        public void AssignTask()
        {
            Console.WriteLine("Input ID for the Task you want to assign.");
            Console.Write("Task Id: ");
            int taskId;
            bool validListId = Int32.TryParse(Console.ReadLine(), out taskId);
            Console.WriteLine("Input ID of the User you want to assign task.");
            Console.Write("User Id: ");
            int userId;
            bool validId = Int32.TryParse(Console.ReadLine(), out userId);

            if (!validListId || taskId <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid Task id.");
                Console.ResetColor();
                return;
            }
            if (!validId || userId <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid User id.");
                Console.ResetColor();
                return;
            }

            AssignTask(taskId, userId);
        }

        public void CompleteTask()
        {
            Console.WriteLine("Input ID for the Task you want to complete.");
            Console.Write("Task Id: ");
            int taskId;
            bool validListId = Int32.TryParse(Console.ReadLine(), out taskId);
            if (!validListId || taskId <= 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid Task id.");
                Console.ResetColor();
                return;
            }
            CompleteTask(taskId);
        }

        public bool CreateTask(int listId, string title, string description)
        {
            int currentUserId = UserService.currentUser.Id;

            TaskToDo newTask = new TaskToDo(listId, title, description, false, currentUserId);
            newTask.UserIdAccessTask.Add(currentUserId);
            UserService.currentUser.AccessibleTasks.Add(newTask.Id);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Task was created.");
            Console.ResetColor();

            newTask.Id = _taskToDorepository.Create(newTask);
            var taskUserAccess = new TaskUserAccess(currentUserId, newTask.Id);
            _taskUserAccessRepository.Create(taskUserAccess);
            taskList.Add(newTask);

            return true;
        }

        public bool EditTask(int id, string title, string description, bool isComplete)
        {
            int currentUserId = UserService.currentUser.Id;
            TaskToDo editTask = taskList.Find(t => t.Id == id);
            if (editTask == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Task does not exist.");
                Console.ResetColor();
                return false;
            }
            else if (!editTask.UserIdAccessTask.Contains(currentUserId))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User does not have permission to edit task.");
                Console.ResetColor();
                return false;
            }
            else
            {
                editTask.Title = title;
                editTask.Description = description;
                editTask.ModifiedDate = DateTime.Now;
                editTask.ModifierId = currentUserId;
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Task with id: {id} is edited.");
            Console.ResetColor();
            _taskToDorepository.Update(editTask);

            return true;
        }

        public bool RemoveTask(int id)
        {
            int currentUserId = UserService.currentUser.Id;
            TaskToDo removedTask = taskList.Find(t => t.Id == id);

            if (removedTask == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Task does not exist.");
                Console.ResetColor();
                return false;
            }
            else if (!removedTask.UserIdAccessTask.Contains(currentUserId))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User doesn't have that task.");
                Console.ResetColor();
                return false;
            }

            var isRemoved = default(bool);
            if (removedTask.CreatorId == currentUserId)
            {
                isRemoved = RemoveLinks(currentUserId, removedTask, removedTask.UserIdAccessTask.ToArray());

                isRemoved = isRemoved && taskList.Remove(removedTask);
                _taskToDorepository.Remove(removedTask);
            }
            else
            {
                isRemoved = RemoveLinks(currentUserId, removedTask, currentUserId);
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Task is removed.");
            Console.ResetColor();
            return isRemoved;
        }

        private bool RemoveLinks(int currentUserId, TaskToDo removedTask, params int[] linksToRemove)
        {
            var isRemoved = default(bool);
            foreach (var userId in linksToRemove)
            {
                var taskUserAccess = new TaskUserAccess(userId, removedTask.Id);
                _taskUserAccessRepository.Remove(taskUserAccess);
            }

            isRemoved = removedTask.UserIdAccessTask.Remove(currentUserId) &&
            UserService.currentUser.AccessibleTasks.Remove(removedTask.Id);

            return isRemoved;
        }

        public bool AssignTask(int taskId, int userId)
        {
            int currentUserId = UserService.currentUser.Id;
            TaskToDo task = taskList.Find(t => t.Id == taskId && t.UserIdAccessTask.Contains(currentUserId));

            if (task == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Task doesn't exist or it is inaccessible.");
                Console.ResetColor();
                return false;
            }
            else if (!ToDoListService.listCollection.Any(l => l.Id == task.ListId && l.UserAccessList.Contains(userId)))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"User with ID: {userId} does not have access to the list.");
                Console.ResetColor();
                return false;
            }
            else if (task.UserIdAccessTask.Contains(userId))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Task is already assigned to user.");
                Console.ResetColor();
                return false;
            }
            else
            {
                task.ModifierId = currentUserId;
                task.UserIdAccessTask.Add(userId);
                var user = UserService.userList.SingleOrDefault(u => u.Id == userId);
                user.AccessibleTasks.Add(taskId);
                var taskUserAccess = new TaskUserAccess(userId, taskId);
                _taskUserAccessRepository.Create(taskUserAccess);
                _taskToDorepository.Update(task);

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Task: {taskId} is assigned to User with id: {userId}.");
                Console.ResetColor();
                return true;
            }
        }

        public bool CompleteTask(int taskId)
        {
            int currentUserId = UserService.currentUser.Id;
            TaskToDo task = taskList.Find(t => t.Id == taskId && t.UserIdAccessTask.Contains(currentUserId));
            var taskExist = taskList.Exists(t => t.Id == taskId);

            if (!taskList.Any(t => t.UserIdAccessTask.Contains(currentUserId)))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User is not authorised to complete this task.");
                Console.ResetColor();
                return false;
            }
            else if (!taskExist)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("No such task.");
                Console.ResetColor();
                return false;
            }
            task.ModifierId = currentUserId;
            task.IsComplete = true;
            _taskToDorepository.Update(task);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Task: {taskId} is completed.");
            Console.ResetColor();
            return true;
        }
    }
}
