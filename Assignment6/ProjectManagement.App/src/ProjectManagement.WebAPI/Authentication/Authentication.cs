﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using ProjectManagement.Models.Entities;
using ProjectManagement.Services.Contracts;
using System.Linq;

namespace ProjectManagement.WebAPI.Authentication
{
    public static class Authentication
    {
        public static User GetCurrentUser(this IUserManagement userService, HttpRequest request)
        {
            request.Headers.TryGetValue("UserId", out StringValues userIdValue);
            if (userIdValue.Count != 0)
            {
                int userId;
                bool isParsed = int.TryParse(userIdValue.First(), out userId);

                if (isParsed)
                {
                    return userService.GetById(userId);
                }
            }

            return null;
        }
    }
}
