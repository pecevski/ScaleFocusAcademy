﻿using Microsoft.AspNetCore.Mvc;
using ProjectManagement.Models.Entities;
using ProjectManagement.Models.Enums;
using ProjectManagement.Services.Contracts;
using ProjectManagement.Services.DTO.Requests;
using ProjectManagement.Services.DTO.Responses;
using ProjectManagement.WebAPI.Authentication;
using ProjectManagement.WebAPI.Mappers;
using System.Collections.Generic;
using System.Linq;

namespace ProjectManagement.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectManagement _projectService;
        private readonly ITeamManagement _teamService;
        private readonly IUserManagement _userService;

        public ProjectsController(IProjectManagement projectService, ITeamManagement teamService, IUserManagement userService)
        {
            _projectService = projectService;
            _teamService = teamService;
            _userService = userService;
        }


        [HttpGet]
        [Route("All")]
        public ActionResult<IEnumerable<ProjectResponse>> GetAll()
        {
            User currentUser = _userService.GetCurrentUser(Request);
            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            if (ModelState.IsValid)
            {
                List<ProjectResponse> projects = new List<ProjectResponse>();

                List<Project> allProjects = _projectService.GetAll().ToList();

                foreach (var project in allProjects)
                {
                    ProjectResponse projectResponse = ProjectMapper.MapProject(project);
                    projects.Add(projectResponse);
                }

                return projects;
            }

            return NoContent();
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<ProjectResponse> Get(int id)
        {
            User currentUser = _userService.GetCurrentUser(Request);
            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            Project project = _projectService.GetById(id);

            if (project == null)
            {
                return NotFound();
            }

            return ProjectMapper.MapProject(project);
        }

        [HttpPost]
        public IActionResult Create(ProjectRequest projectRequest)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            Project project = ProjectMapper.MapProjectRequest(projectRequest);
            project.Owner = currentUser;

            if (project == null)
            {
                return NotFound();
            }

            bool isCreated = _projectService.Create(project);

            if (isCreated && ModelState.IsValid)
            {
                Project projectFromDB = _projectService.GetById(project.Id);
                return CreatedAtAction("Get", "Projects", new { id = projectFromDB.Id }, null);
            }

            return BadRequest("Project already exist.");
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult Update(int id, ProjectRequest projectRequest)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            Project projectFromDB = _projectService.GetById(id);


            if (projectFromDB == null)
            {
                return NotFound();
            }

            if (projectFromDB.Owner != currentUser)
            {
                return Unauthorized();
            }

            if (ModelState.IsValid)
            {
                Project updateProject = ProjectMapper.MapProjectRequest(projectRequest);
                _projectService.Update(id, updateProject);
                return Ok("Project updated.");
            }

            return NoContent();
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            Project project = _projectService.GetById(id);

            if (project == null)
            {
                return NotFound();
            }

            if (project.Owner != currentUser)
            {
                return Unauthorized();
            }

            if (_projectService.Delete(project.Id))
            {
                return Ok("Project deleted.");
            }

            return NoContent();
        }

        [HttpPut]
        [Route("Assign/{teamId}")]
        public IActionResult AssigningTeamToProject(int teamId, int projectId)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            Team team = _teamService.GetById(teamId);
            Project project = _projectService.GetById(projectId);
            if (team == null || project == null)
            {
                return BadRequest();
            }
            if (project.Owner == currentUser)
            {
                _projectService.AssignTeamToProject(team, projectId, currentUser);
                return Ok("Team assigned to Project.");
            }

            return NoContent();
        }
    }
}
