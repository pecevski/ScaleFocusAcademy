﻿using Microsoft.AspNetCore.Mvc;
using ProjectManagement.Models.Entities;
using ProjectManagement.Models.Enums;
using ProjectManagement.Services.Contracts;
using ProjectManagement.Services.DTO.Requests;
using ProjectManagement.Services.DTO.Responses;
using ProjectManagement.WebAPI.Authentication;
using ProjectManagement.WebAPI.Mappers;
using System.Collections.Generic;
using System.Linq;

namespace ProjectManagement.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserManagement _userService;
        public UsersController(IUserManagement userService)
        {
            _userService = userService;
        }

        [HttpPost]
        [Route("Login")]
        public ActionResult<int> Login(string username, string password)
        {
            User userFromDB = _userService.LogIn(username, password);

            if (userFromDB == null)
            {
                return Unauthorized();
            }

            return Ok(userFromDB.Id);
        }

        [HttpGet]
        [Route("All")]
        public ActionResult<IEnumerable<UserResponse>> GetAll()
        {
            User currentUser = _userService.GetCurrentUser(Request);
            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            if (ModelState.IsValid)
            {
                List<UserResponse> users = new List<UserResponse>();

                List<User> allUsers = _userService.GetAll().ToList();

                foreach (var user in allUsers)
                {
                    UserResponse userResponse = UserMapper.MapUser(user);
                    users.Add(userResponse);
                }

                return users;
            }

            return NoContent();
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<UserResponse> Get(int id)
        {
            User currentUser = _userService.GetCurrentUser(Request);
            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            User user = _userService.GetById(id);

            if (user == null)
            {
                return NotFound();
            }

            return UserMapper.MapUser(user);
        }

        [HttpPost]
        public IActionResult Create(UserRequest userRequest)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            User user = UserMapper.MapUserRequest(userRequest);

            if (user == null)
            {
                return NotFound();
            }

            bool isCreated = _userService.Create(user);

            if (isCreated && ModelState.IsValid)
            {
                User userFromDB = _userService.GetById(user.Id);

                return CreatedAtAction("Get", "Users", new { id = userFromDB.Id }, null);
            }

            return BadRequest("User already exist");
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult Update(int id, UserRequest userRequest)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            if (ModelState.IsValid)
            {
                var updateUser = UserMapper.MapUserRequest(userRequest);
                _userService.Update(id, updateUser);
                return Ok("User updated");
            }

            return NoContent();
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            User user = _userService.GetById(id);

            if (user == null)
            {
                return NotFound();
            }

            if (_userService.Delete(user.Id))
            {
                return Ok();
            }

            return NoContent();
        }
    }
}
