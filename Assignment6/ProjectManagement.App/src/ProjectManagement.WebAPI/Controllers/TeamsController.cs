﻿using Microsoft.AspNetCore.Mvc;
using ProjectManagement.Models.Entities;
using ProjectManagement.Models.Enums;
using ProjectManagement.Services.Contracts;
using ProjectManagement.Services.DTO.Requests;
using ProjectManagement.Services.DTO.Responses;
using ProjectManagement.WebAPI.Authentication;
using ProjectManagement.WebAPI.Mappers;
using System.Collections.Generic;
using System.Linq;

namespace ProjectManagement.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamManagement _teamService;
        private readonly IUserManagement _userService;

        public TeamsController(ITeamManagement teamService, IUserManagement userService)
        {
            _teamService = teamService;
            _userService = userService;
        }

        [HttpGet]
        [Route("All")]
        public ActionResult<IEnumerable<TeamResponse>> GetAll()
        {
            User currentUser = _userService.GetCurrentUser(Request);
            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            if (ModelState.IsValid)
            {
                List<TeamResponse> teams = new List<TeamResponse>();

                List<Team> allTeams = _teamService.GetAll().ToList();

                foreach (var team in allTeams)
                {
                    TeamResponse TeamResponse = TeamMapper.MapTeam(team);
                    teams.Add(TeamResponse);
                }

                return teams;
            }

            return NoContent();
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<TeamResponse> Get(int id)
        {
            User currentUser = _userService.GetCurrentUser(Request);
            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            Team team = _teamService.GetById(id);

            if (team == null)
            {
                return NotFound();
            }

            return TeamMapper.MapTeam(team);
        }

        [HttpPost]
        public IActionResult Create(TeamRequest teamRequest)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            Team team = TeamMapper.MapTeamRequest(teamRequest);
            team.TeamCreator = currentUser;

            if (team == null)
            {
                return NotFound();
            }

            bool isCreated = _teamService.Create(team);

            if (isCreated && ModelState.IsValid)
            {
                Team teamFromDB = _teamService.GetById(team.Id);
                return CreatedAtAction("Get", "Teams", new { id = teamFromDB.Id }, null);
            }

            return BadRequest("Team already exist.");
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult Update(int id, TeamRequest teamRequest)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            Team team = _teamService.GetById(id);

            if (team == null)
            {
                return NotFound();
            }

            if (team.TeamCreator != currentUser)
            {
                return Unauthorized();
            }

            if (ModelState.IsValid)
            {
                var updateTeam = TeamMapper.MapTeamRequest(teamRequest);
                _teamService.Update(id, updateTeam);
                return Ok("Team updated.");
            }

            return NoContent();
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            Team team = _teamService.GetById(id);

            if (team == null)
            {
                return NotFound();
            }

            if (team.TeamCreator != currentUser)
            {
                return Unauthorized();
            }

            if (_teamService.Delete(team.Id))
            {
                return Ok("Team deleted.");
            }

            return NoContent();
        }

        [HttpPut]
        [Route("Assign/{userId}")]
        public IActionResult AssignUserToTeams(int userId, int teamId)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            User user = _userService.GetById(userId);
            Team team = _teamService.GetById(teamId);

            if (user == null)
            {
                return NotFound();
            }
            if (team.TeamCreator == currentUser)
            {
                _teamService.AssignUserToTeams(user, teamId);
                return Ok("User is assigned to Team.");
            }

            return NoContent();
        }

        [HttpPut]
        [Route("Remove/{userId}")]
        public IActionResult RemoveUserFromTeams(int userId, int teamId)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            User user = _userService.GetById(userId);
            Team team = _teamService.GetById(teamId);

            if (user == null)
            {
                return NotFound();
            }
            if (team.TeamCreator == currentUser)
            {
                _teamService.RemoveUserFromTeams(user, teamId);
                return Ok("User is removed from Team.");
            }

            return NoContent();
        }

    }
}
