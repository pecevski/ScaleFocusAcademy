﻿using Microsoft.AspNetCore.Mvc;
using ProjectManagement.Models.Entities;
using ProjectManagement.Models.Enums;
using ProjectManagement.Services.Contracts;
using ProjectManagement.Services.DTO.Requests;
using ProjectManagement.Services.DTO.Responses;
using ProjectManagement.WebAPI.Authentication;
using ProjectManagement.WebAPI.Mappers;
using System.Collections.Generic;
using System.Linq;

namespace ProjectManagement.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectTasksController : ControllerBase
    {
        private readonly IProjectManagement _projectService;
        private readonly ITaskManagement _taskService;
        private readonly IUserManagement _userService;

        public ProjectTasksController(IProjectManagement projectService, ITaskManagement taskService, IUserManagement userService)
        {
            _projectService = projectService;
            _taskService = taskService;
            _userService = userService;
        }

        [HttpGet]
        [Route("All")]
        public ActionResult<List<ProjectTaskResponse>> GetAll(int projectId)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            Project tasksFromProject = _projectService.GetById(projectId);

            if (tasksFromProject == null)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                List<ProjectTaskResponse> projectTask = new List<ProjectTaskResponse>();

                List<ProjectTask> allTasks = _taskService.GetAll(projectId).ToList();

                foreach (var task in allTasks)
                {
                    ProjectTaskResponse projectTaskResponse = TaskMapper.MapTask(task);
                    projectTask.Add(projectTaskResponse);
                }

                return projectTask;
            }

            return NoContent();
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<ProjectTaskResponse> Get(int id)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            ProjectTask tasksFromDB = _taskService.GetById(id);

            if (tasksFromDB == null)
            {
                return NotFound();
            }

            return TaskMapper.MapTask(tasksFromDB);
        }

        [HttpPost]
        public IActionResult Create(int projectId, ProjectTaskRequest createTask)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            Project tasksFromProject = _projectService.GetById(projectId);

            if (tasksFromProject == null)
            {
                return NotFound();
            }

            ProjectTask task = TaskMapper.MapTaskRequest(createTask);
            task.TaskCreator = currentUser;
            task.Project = tasksFromProject;

            bool isCreated = _taskService.Create(task);

            if (isCreated && ModelState.IsValid)
            {

                return CreatedAtAction("Get", "ProjectTasks", new { id = task.Id }, null);
            }

            return BadRequest();
        }

        [HttpPut]
        public IActionResult Update(ProjectTaskRequest taskRequest, int taskId)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            if (currentUser.Role != Role.Admin)
            {
                return Unauthorized();
            }

            ProjectTask tasksFromDB = _taskService.GetById(taskId);

            if (tasksFromDB == null)
            {
                return NotFound();
            }

            if(tasksFromDB.TaskCreatorId != currentUser.Id)
            {
                return Unauthorized();
            }

            if (ModelState.IsValid)
            {
                ProjectTask updateTask = TaskMapper.MapTaskRequest(taskRequest);
                _taskService.Update(taskId, updateTask);
                return Ok("Task updated.");
            }

            return NoContent();
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            ProjectTask tasksFromDB = _taskService.GetById(id);

            if (tasksFromDB == null)
            {
                return NotFound();
            }

            if (tasksFromDB.TaskCreatorId != currentUser.Id)
            {
                return Unauthorized();
            }

            if (_taskService.Delete(id))
            {
                return Ok();
            }

            return NoContent();
        }

        [HttpPut]
        [Route("Assign")]
        public IActionResult Assign(int taskId, int assignedUserId)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            User assignedUser = _userService.GetById(assignedUserId);

            if (assignedUser == null)
            {
                return NotFound();
            }

            ProjectTask tasksFromDB = _taskService.GetById(taskId);
            
            if (tasksFromDB == null)
            {
                return NotFound();
            }

            if (tasksFromDB.TaskCreatorId != currentUser.Id)
            {
                return Unauthorized();
            }

            bool isAssigned = false;

            if (ModelState.IsValid)
            {
                isAssigned = _taskService.AssignTaskToUser(taskId, assignedUser.Id, currentUser.Id);
            }

            if (isAssigned)
            {
                return Ok("User assigned to Task.");
            }

            return BadRequest();
        }

        [HttpPut]
        [Route("CompleteTask/{taskId}")]
        public IActionResult CompleteTask(int taskId, int assignedUserId)
        {
            User currentUser = _userService.GetCurrentUser(Request);

            if (currentUser == null)
            {
                return BadRequest();
            }

            ProjectTask tasksFromDB = _taskService.GetById(taskId);
           
            if (tasksFromDB == null)
            {
                return NotFound();
            }

            if(tasksFromDB.TaskCreatorId != currentUser.Id && tasksFromDB.AssignedUserId != assignedUserId)
            {
                return Unauthorized();
            }

            bool isComplete = false;

            if (ModelState.IsValid)
            {
                isComplete = _taskService.CompleteTask(taskId, currentUser.Id, assignedUserId);
            }

            if (isComplete)
            {
                return Ok("Task completed.");
            }

            return BadRequest();
        }
    }
}
