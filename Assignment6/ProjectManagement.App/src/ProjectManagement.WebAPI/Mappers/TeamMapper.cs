﻿using ProjectManagement.Models.Entities;
using ProjectManagement.Services.DTO.Requests;
using ProjectManagement.Services.DTO.Responses;

namespace ProjectManagement.WebAPI.Mappers
{
    public static class TeamMapper
    {
        public static TeamResponse MapTeam(Team team)
        {
            var teamResponse = new TeamResponse()
            {
                Id = team.Id,
                TeamName = team.TeamName,
            };
            return teamResponse;
        }

        public static Team MapTeamRequest(TeamRequest teamRequest)
        {
            var team = new Team()
            {
                TeamName = teamRequest.TeamName,
            };
            return team;
        }
    }
}
