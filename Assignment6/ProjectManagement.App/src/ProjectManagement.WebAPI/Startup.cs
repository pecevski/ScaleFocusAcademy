using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using ProjectManagement.Data;
using ProjectManagement.Services;
using ProjectManagement.Services.Contracts;
using ProjectManagement.WebAPI.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectManagement.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<PMDataBaseContext>(options => options.UseSqlServer(Configuration.GetConnectionString("Default")),
                                                       ServiceLifetime.Scoped);

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Project Management", Version = "v1.0" });
                c.OperationFilter<UserHeaderFilter>();
            });

            services.AddScoped<PMDataBaseContext>();
            services.AddTransient<IUserManagement, UserManagement>();
            services.AddTransient<ITeamManagement, TeamManagement>();
            services.AddTransient<IProjectManagement, ProjectsManagement>();
            services.AddTransient<ITaskManagement, TaskManagement>();
            services.AddTransient<IWorkLogManagement, WorkLogManagement>();
            services.AddTransient<IDBInitializer, DBInitializer>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDBInitializer initializer)
        {
            initializer.Initialize();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Project Management v1.0"));
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
