﻿using ProjectManagement.Data;
using ProjectManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ProjectManagement.Services.Tests
{
    public class TeamManagementTests : BaseTestEnvironment
    {
       
        [Fact]
        public void GetAll_Teams_ReturnTeams()
        {
            //arrange
            var sut = new TeamManagement(_dataBaseContext);
            var listOfTeams = _dataBaseContext.Teams;

            //act
            var result = sut.GetAll();

            //assert
            Assert.Equal(listOfTeams, result);
        }

        [Fact]
        public void GetById_ValidId_ReturnTeamById()
        {
            //arrange
            var sut = new TeamManagement(_dataBaseContext);
            var fakeId = 1;
            var testTeam = _dataBaseContext.Teams.Find(fakeId);

            //act
            var result = sut.GetById(1);

            //assert
            Assert.Equal(testTeam.Id, result.Id);
        }

        [Fact]
        public void GetById_NotValidId_ReturnNull()
        {
            //arrange
            var sut = new TeamManagement(_dataBaseContext);

            //act
            var result = sut.GetById(11);

            //assert
            Assert.Null(result);
        }

        [Fact]
        public void Create_Team_ReturnTrue()
        {
            //arrange
            var sut = new TeamManagement(_dataBaseContext);
            var fakeTeam = new Team()
            {
                TeamCreatorId = 1,
                TeamName = "FakeTeam",
            };

            //act
            bool result = sut.Create(fakeTeam);
            sut.Delete(fakeTeam.Id);

            //assert
            Assert.True(result);
        }

        [Fact]
        public void Create_Team_ReturnFalse()
        {
            //arrange
            var sut = new TeamManagement(_dataBaseContext);
            var teamname = "fake-Teamname";
           
            var testTeam = new Team
            {
                TeamCreatorId = 1,
                TeamName = teamname,
            };

            //act
            sut.Create(testTeam);
            var result = sut.Create(testTeam);

            //assert
            Assert.False(result);
        }

        [Fact]
        public void Update_Team_ReturnTrue()
        {
            //arrange
            var sut = new TeamManagement(_dataBaseContext);
            var name = "TestName";
            var fakeEditTeam = new Team
            {
                TeamCreatorId = 1,
                TeamName = name,
            };

            //act
            sut.Create(fakeEditTeam);
            var result = sut.Update(fakeEditTeam.Id, fakeEditTeam);
            sut.Delete(fakeEditTeam.Id);

            //assert
            Assert.True(result);
        }

        [Fact]
        public void Update_Team_ReturnFalse()
        {
            //arrange
            var sut = new TeamManagement(_dataBaseContext);
            var fakeEditTeam = new Team
            {
                TeamCreatorId = 1,
                TeamName = "FakeTeam",
            };
            var fakeId = fakeEditTeam.Id;

            //act
            var result = sut.Update(fakeId, fakeEditTeam);

            //assert
            Assert.False(result);
        }
    }
}
