﻿using ProjectManagement.Data;
using ProjectManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ProjectManagement.Services.Tests
{
    public class ProjectManagementTests : BaseTestEnvironment
    {
        [Fact]
        public void GetAll_Projects_ReturnProjects()
        {
            //arrange
           
            var sut = new ProjectsManagement(_dataBaseContext);
            var listOfProjects = _dataBaseContext.Projects;

            //act
            var result = sut.GetAll();

            //assert
            Assert.Equal(listOfProjects, result);
        }

        [Fact]
        public void GetById_ValidId_ReturnProjectById()
        {
            //arrange
            var sut = new ProjectsManagement(_dataBaseContext);
            var fakeId = 1;
            var testProject = _dataBaseContext.Projects.Find(fakeId);

            //act
            var result = sut.GetById(1);

            //assert
            Assert.Equal(testProject.Id, result.Id);
        }

        [Fact]
        public void GetById_NotValidId_ReturnNull()
        {
            //arrange
            var sut = new ProjectsManagement(_dataBaseContext);

            //act
            var result = sut.GetById(13);

            //assert
            Assert.Null(result);
        }

        [Fact]
        public void Create_Project_ReturnTrue()
        {
            //arrange
            var sut = new ProjectsManagement(_dataBaseContext);
            var fakeProject = new Project()
            {
                OwnerId = 1,
                Title = "ProjectTitle",
            };

            //act
            bool result = sut.Create(fakeProject);
            sut.Delete(fakeProject.Id);

            //assert
            Assert.True(result);
        }

        [Fact]
        public void Create_Project_ReturnFalse()
        {
            //arrange
            var sut = new ProjectsManagement(_dataBaseContext);

            var testProject = new Project
            {
                OwnerId = 1,
                Title = "ProjectName",
            };

            //act
            sut.Create(testProject);
            var result = sut.Create(testProject);
            sut.Delete(testProject.Id);

            //assert
            Assert.False(result);
        }

        [Fact]
        public void Update_Project_ReturnTrue()
        {
            //arrange
            var sut = new ProjectsManagement(_dataBaseContext);
            var fakeEditProject = new Project
            {
                OwnerId = 1,
                Title = "ProjectFake",
            };

            //act
            sut.Create(fakeEditProject);
            var result = sut.Update(fakeEditProject.Id, fakeEditProject);
            sut.Delete(fakeEditProject.Id);

            //assert
            Assert.True(result);
        }

        [Fact]
        public void Update_Project_ReturnFalse()
        {
            //arrange
            var sut = new ProjectsManagement(_dataBaseContext);
            var fakeEditProject = new Project
            {
                OwnerId = 1,
                Title = "ProjectTitle",
            };
            var fakeId = fakeEditProject.Id;

            //act
            var result = sut.Update(fakeId, fakeEditProject);

            //assert
            Assert.False(result);
        }
    }
}
