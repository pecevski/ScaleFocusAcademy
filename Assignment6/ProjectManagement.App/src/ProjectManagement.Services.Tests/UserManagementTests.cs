using Microsoft.EntityFrameworkCore;
using Moq;
using ProjectManagement.Data;
using ProjectManagement.Models.Entities;
using ProjectManagement.Models.Enums;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Xunit;

namespace ProjectManagement.Services.Tests
{
    public class UserManagementTests : BaseTestEnvironment
    {
        [Fact]
        public void Login_ValidateUser_ReturnUser()
        {
            //arrange
            var sut = new UserManagement(_dataBaseContext);
            var username = "admin";
            var password = "adminpass";
            var testUser = new User
            {
                Username = username,
                Password = password
            };

            //act
            var result = sut.LogIn(username, password);

            //assert
            Assert.Equal(testUser.Username, result.Username);
        }

        [Fact]
        public void GetAll_Users_ReturnUsers()
        {
            //arrange
            var sut = new UserManagement(_dataBaseContext);
            var listOfUsers = _dataBaseContext.Users;

            //act
            var result = sut.GetAll();

            //assert
            Assert.Equal(listOfUsers, result);
        }

        [Fact]
        public void GetById_ValidId_ReturnUserById()
        {
            //arrange
            var sut = new UserManagement(_dataBaseContext);
            var fakeId = 1;
            var testUser = _dataBaseContext.Users.Find(fakeId);

            //act
            var result = sut.GetById(1);

            //assert
            Assert.Equal(testUser.Id, result.Id);
        }

        [Fact]
        public void GetById_NotValidId_ReturnNull()
        {
            //arrange
            var sut = new UserManagement(_dataBaseContext);

            //act
            var result = sut.GetById(-1);

            //assert
            Assert.Null(result);
        }

        [Fact]
        public void Create_User_ReturnTrue()
        {
            //arrange
            var sut = new UserManagement(_dataBaseContext);
            var testUser = new User()
            {
                Username = "user",
                Password = "password",
                FirstName = "name",
                LastName = "lastname",
                Role = (Role)2,
            };

            //act
            bool result = sut.Create(testUser);
            sut.Delete(testUser.Id);

            //assert
            Assert.True(result);
        }

        [Fact]
        public void Create_ExistingUser_ReturnFalse()
        {
            //arrange
            var sut = new UserManagement(_dataBaseContext);
            var username = "fake-username";
            var password = "fake-password";
            var firstname = "fake-name";
            var lastName = "fake-lastname";
            var testUser = new User
            {
                Username = username,
                Password = password,
                FirstName = firstname,
                LastName = lastName,
            };

            //act
            sut.Create(testUser);
            var result = sut.Create(testUser);
            sut.Delete(testUser.Id);

            //assert
            Assert.False(result);
        }

        [Fact]
        public void Update_User_ReturnTrue()
        {
            //arrange
            var sut = new UserManagement(_dataBaseContext);
            var name = "TestName";
            var fakeEditUser = new User
            {
                Username = name,
                Password = name,
                FirstName = name,
                LastName = name,
            };

            //act
            sut.Create(fakeEditUser);
            var result = sut.Update(fakeEditUser.Id, fakeEditUser);
            sut.Delete(fakeEditUser.Id);

            //assert
            Assert.True(result);
        }

        [Fact]
        public void Update_User_ReturnFalse()
        {
            //arrange
            var sut = new UserManagement(_dataBaseContext);
            var name = "TestName";
            var fakeEditUser = new User
            {
                Username = name,
                Password = name,
                FirstName = name,
                LastName = name,
            };
            var fakeId = fakeEditUser.Id;

            //act
            var result = sut.Update(fakeId, fakeEditUser);

            //assert
            Assert.False(result);
        }
    }
}
