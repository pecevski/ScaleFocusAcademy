﻿using Microsoft.EntityFrameworkCore;
using ProjectManagement.Data;
using ProjectManagement.Models.Entities;

namespace ProjectManagement.Services.Tests
{
    public class BaseTestEnvironment
    {
        protected readonly PMDataBaseContext _dataBaseContext;

        public BaseTestEnvironment()
        {
            var optionsBuilder = new DbContextOptionsBuilder<PMDataBaseContext>();
            var connectionString = "Server=(LocalDb)\\MSSQLLocalDB;Database=TestingProjectDB;Trusted_Connection=True;";
            optionsBuilder.UseSqlServer(connectionString);
            _dataBaseContext = new PMDataBaseContext(optionsBuilder.Options);
            DBInitializer initializer = new DBInitializer(_dataBaseContext);
            initializer.Initialize();
        }
    }
}