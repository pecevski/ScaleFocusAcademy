﻿using ProjectManagement.Data;
using ProjectManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ProjectManagement.Services.Tests
{
    public class TaskManagementTests : BaseTestEnvironment
    {
        [Fact]
        public void GetAll_Tasks_ReturnTasks()
        {
            //arrange
            var sut = new TaskManagement(_dataBaseContext);
            var projectFakeId = 1;
            var listOfTasks = _dataBaseContext.ProjectTasks.Where(t => t.ProjectId == projectFakeId).ToList();

            //act
            var result = sut.GetAll(projectFakeId);

            //assert
            Assert.Equal(listOfTasks, result);
        }

        [Fact]
        public void GetById_ValidId_ReturnTaskById()
        {
            //arrange
            var sut = new TaskManagement(_dataBaseContext);
            var fakeId = 3;
            var testTask = _dataBaseContext.ProjectTasks.Find(fakeId);

            //act
            var result = sut.GetById(3);

            //assert
            Assert.Equal(testTask.Id, result.Id);
        }

        [Fact]
        public void GetById_NotValidId_ReturnNull()
        {
            //arrange
            var sut = new TaskManagement(_dataBaseContext);

            //act
            var result = sut.GetById(10);

            //assert
            Assert.Null(result);
        }

        [Fact]
        public void Create_Task_ReturnTrue()
        {
            //arrange
            var sut = new TaskManagement(_dataBaseContext);
            var fakeTask = new ProjectTask()
            {
                TaskCreatorId = 1,
                ProjectId = 1,
                Title = "NewTask",
            };

            //act
            bool result = sut.Create(fakeTask);
            sut.Delete(fakeTask.Id);

            //assert
            Assert.True(result);
        }

        [Fact]
        public void Create_Task_ReturnFalse()
        {
            //arrange
            
            var sut = new TaskManagement(_dataBaseContext);

            var testTask = new ProjectTask
            {
                TaskCreatorId = 1,
                ProjectId = 1,
                Title = "TaskName",
            };

            //act
            sut.Create(testTask);
            var result = sut.Create(testTask);
            sut.Delete(testTask.Id);

            //assert
            Assert.False(result);
        }

        [Fact]
        public void Update_ProjectTask_ReturnTrue()
        {
            //arrange
            var sut = new TaskManagement(_dataBaseContext);
            var fakeEditProjectTask = new ProjectTask
            {
                TaskCreatorId = 1,
                ProjectId = 1,
                Title = "NewTask",
            };

            //act
            sut.Create(fakeEditProjectTask);
            var result = sut.Update(fakeEditProjectTask.Id, fakeEditProjectTask);
            sut.Delete(fakeEditProjectTask.Id);

            //assert
            Assert.True(result);
        }

        [Fact]
        public void Update_ProjectTask_ReturnFalse()
        {
            //arrange
            var sut = new TaskManagement(_dataBaseContext);
            var fakeEditProjectTask = new ProjectTask
            {
                TaskCreatorId = 1,
                ProjectId = 1,
                Title = "FakeTask",
            };
            var fakeId = fakeEditProjectTask.Id;

            //act
            var result = sut.Update(fakeId, fakeEditProjectTask);

            //assert
            Assert.False(result);
        }
    }
}
