﻿using ProjectManagement.Data;
using ProjectManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ProjectManagement.Services.Tests
{
    public class WorkLogManagementTests : BaseTestEnvironment
    {
        [Fact]
        public void GetAll_WorkLog_ReturnWorkLogs()
        {
            //arrange
            var sut = new WorkLogManagement(_dataBaseContext);
            var fakeTaskId = 1;
            var listOfLogs = _dataBaseContext.WorkLogs.Where(w => w.TaskId == fakeTaskId).ToList();

            //act
            var result = sut.GetAll(fakeTaskId);

            //assert
            Assert.Equal(listOfLogs, result);
        }

        [Fact]
        public void GetById_ValidId_ReturnLogById()
        {
            //arrange
            var sut = new WorkLogManagement(_dataBaseContext);
            var fakeId = 8;
            var testLog = _dataBaseContext.WorkLogs.Find(fakeId);

            //act
            var result = sut.GetById(8);

            //assert
            Assert.Equal(testLog.Id, result.Id);
        }

        [Fact]
        public void GetById_NotValidId_ReturnNull()
        {
            //arrange
            var sut = new WorkLogManagement(_dataBaseContext);

            //act
            var result = sut.GetById(15);

            //assert
            Assert.Null(result);
        }

        [Fact]
        public void Create_WorkLogt_ReturnTrue()
        {
            //arrange
            var sut = new WorkLogManagement(_dataBaseContext);
            var testWorkLog = new WorkLog()
            {
                UserId = 1,
                TaskId = 3,
                WorkingHours = 5,
                WorkingPeriod = DateTime.UtcNow,
            };

            //act
            bool result = sut.Create(testWorkLog);
            sut.Delete(testWorkLog.Id);

            //assert
            Assert.True(result);
        }

        [Fact]
        public void Create_WorkLog_ReturnFalse()
        {
            //arrange
            var sut = new WorkLogManagement(_dataBaseContext);

            var testLog = new WorkLog
            {
                UserId = 1,
                TaskId = 3,
                WorkingHours = 3,
                WorkingPeriod = DateTime.UtcNow,
            };

            //act
            sut.Create(testLog);
            var result = sut.Create(testLog);
            sut.Delete(testLog.Id);

            //assert
            Assert.False(result);
        }

        [Fact]
        public void Update_WorkLog_ReturnTrue()
        {
            //arrange
            var sut = new WorkLogManagement(_dataBaseContext);
            var fakeEditWorkLog = new WorkLog
            {
                UserId = 1,
                TaskId = 3,
                WorkingHours = 3,
                WorkingPeriod = DateTime.UtcNow,
            };

            //act
            sut.Create(fakeEditWorkLog);
            var result = sut.Update(fakeEditWorkLog.Id, fakeEditWorkLog);
            sut.Delete(fakeEditWorkLog.Id);

            //assert
            Assert.True(result);
        }

        [Fact]
        public void Update_WorkLog_ReturnFalse()
        {
            //arrange
            var sut = new WorkLogManagement(_dataBaseContext);
            var fakeEditWorkLog = new WorkLog
            {
                UserId = 1,
                TaskId = 1,
                WorkingHours = 6,
                WorkingPeriod = DateTime.UtcNow,
            };
            var fakeId = fakeEditWorkLog.Id;

            //act
            var result = sut.Update(fakeId, fakeEditWorkLog);

            //assert
            Assert.False(result);
        }
    }
}
