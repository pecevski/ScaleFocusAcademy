﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectManagement.Services.DTO.Requests
{
    public class WorkLogRequest
    {
        [Required]
        [Range(0.5, 24)]
        public float WorkingHours { get; set; }
        [Required]
        public DateTime WorkingPeriod { get; set; }
    }
}
