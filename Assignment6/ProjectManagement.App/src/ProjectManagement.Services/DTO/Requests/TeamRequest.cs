﻿using System.ComponentModel.DataAnnotations;

namespace ProjectManagement.Services.DTO.Requests
{
    public class TeamRequest
    {
        [Required]
        [MinLength(2)]
        [MaxLength(20)]
        public string TeamName { get; set; }
    }
}
