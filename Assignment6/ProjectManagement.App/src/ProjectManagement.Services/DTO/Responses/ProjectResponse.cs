﻿using System.Collections.Generic;

namespace ProjectManagement.Services.DTO.Responses
{
    public class ProjectResponse
    {
        public ProjectResponse()
        {
            Teams = new List<TeamResponse>();
            ProjectTasks = new List<ProjectTaskResponse>();
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public UserResponse Owner { get; set; }
        public List<ProjectTaskResponse> ProjectTasks { get; set; }
        public List<TeamResponse> Teams { get; set; }

    }
}
