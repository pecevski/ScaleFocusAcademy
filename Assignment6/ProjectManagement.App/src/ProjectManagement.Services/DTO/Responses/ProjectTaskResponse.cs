﻿using ProjectManagement.Models.Enums;
using System.Collections.Generic;

namespace ProjectManagement.Services.DTO.Responses
{
    public class ProjectTaskResponse
    {
        public ProjectTaskResponse()
        {
            WorkLogs = new List<WorkLogResponse>();
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public Status Status { get; set; }
        public ProjectResponse Project { get; set; }
        public UserResponse User { get; set; }
        public List<WorkLogResponse> WorkLogs { get; set; }
    }
}
