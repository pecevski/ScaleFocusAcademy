﻿using System.Collections.Generic;

namespace ProjectManagement.Services.DTO.Responses
{
    public class TeamResponse
    {
        public TeamResponse()
        {
            TeamMembers = new List<UserResponse>();
        }
        public int Id { get; set; }
        public string TeamName { get; set; }
        public ProjectResponse Project { get; set; }
        public List<UserResponse> TeamMembers { get; set; }
    }
}
