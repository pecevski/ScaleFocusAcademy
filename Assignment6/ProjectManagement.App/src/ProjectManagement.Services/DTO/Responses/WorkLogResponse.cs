﻿using System;

namespace ProjectManagement.Services.DTO.Responses
{
    public class WorkLogResponse
    {
        public WorkLogResponse()
        {
            WorkingPeriod = DateTime.UtcNow;
        }
        public int Id { get; set; }
        public DateTime WorkingPeriod { get; set; }
        public float WorkingHours { get; set; }
        public ProjectTaskResponse ProjectTask { get; set; }
        public UserResponse User { get; set; }
    }
}
