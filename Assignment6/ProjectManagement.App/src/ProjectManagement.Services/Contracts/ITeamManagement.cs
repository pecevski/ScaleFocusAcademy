﻿using ProjectManagement.Models.Entities;
using System.Collections.Generic;

namespace ProjectManagement.Services.Contracts
{
    public interface ITeamManagement : IServices<Team>
    {
        IEnumerable<Team> GetAll();
        bool AssignUserToTeams(User user, int teamId);
        bool RemoveUserFromTeams(User user, int teamId);
    }
}
