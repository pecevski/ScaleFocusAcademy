﻿using ProjectManagement.Models.Entities;
using System.Collections.Generic;

namespace ProjectManagement.Services.Contracts
{
    public interface ITaskManagement : IServices<ProjectTask>
    {
        IEnumerable<ProjectTask> GetAll(int projectId);
        bool AssignTaskToUser(int taskId, int userId, int creatorId);
        bool CompleteTask(int taskId, int creatorId, int assignUserId);
    }
}
