﻿using ProjectManagement.Models.Entities;
using System.Collections.Generic;

namespace ProjectManagement.Services.Contracts
{
    public interface IWorkLogManagement : IServices<WorkLog>
    {
        IEnumerable<WorkLog> GetAll(int taskId);
    }
}
