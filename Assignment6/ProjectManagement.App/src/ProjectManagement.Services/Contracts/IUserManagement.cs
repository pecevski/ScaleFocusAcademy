﻿using ProjectManagement.Models.Entities;
using System.Collections.Generic;

namespace ProjectManagement.Services.Contracts
{
    public interface IUserManagement : IServices<User>
    {
        User LogIn(string username, string password);
        IEnumerable<User> GetAll();
    }
}
