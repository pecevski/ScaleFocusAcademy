﻿using ProjectManagement.Data;
using ProjectManagement.Models.Entities;
using ProjectManagement.Services.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace ProjectManagement.Services
{
    public class ProjectsManagement : IProjectManagement
    {
        private readonly PMDataBaseContext _database;

        public ProjectsManagement(PMDataBaseContext database)
        {
            _database = database;
        }
        public IEnumerable<Project> GetAll()
        {
            return _database.Projects.ToList();
        }

        public Project GetById(int id)
        {
            return _database.Projects.SingleOrDefault(p => p.Id == id);
        }

        public bool Create(Project project)
        {
            if (_database.Projects.SingleOrDefault(t => t.Title == project.Title) != null)
            {
                return false;
            }

            _database.Projects.Add(project);
            _database.SaveChanges();

            return true;
        }

        public bool Update(int id, Project project)
        {
            Project updateProject = _database.Projects.Find(id);

            if (updateProject == null)
            {
                return false;
            }

            updateProject.Title = project.Title;

            _database.Projects.Update(updateProject);
            _database.SaveChanges();
            return true;
        }
        public bool Delete(int id)
        {
            Project removeProject = _database.Projects.Find(id);

            if (removeProject == null)
            {
                return false;
            }

            _database.Projects.Remove(removeProject);
            _database.SaveChanges();
            return true;
        }

        public bool AssignTeamToProject(Team team, int projectId, User user)
        {
            Project project = _database.Projects.SingleOrDefault(p => p.Id == projectId);
            Team assignTeam = _database.Teams.SingleOrDefault(t => t.Id == team.Id);

            if (project == null || assignTeam == null || project.Owner != user)
            {
                return false;
            }

            project.Teams.Add(assignTeam);
            _database.SaveChanges();
            return true;
        }
    }
}
