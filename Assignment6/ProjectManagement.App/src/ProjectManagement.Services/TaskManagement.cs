﻿using ProjectManagement.Data;
using ProjectManagement.Models.Entities;
using ProjectManagement.Models.Enums;
using ProjectManagement.Services.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace ProjectManagement.Services
{
    public class TaskManagement : ITaskManagement
    {
        private readonly PMDataBaseContext _database;

        public TaskManagement(PMDataBaseContext database)
        {
            _database = database;
        }

        public IEnumerable<ProjectTask> GetAll(int projectId)
        {
            return _database.ProjectTasks.Where(tp => tp.ProjectId == projectId).ToList();
        }
        public ProjectTask GetById(int id)
        {
            return _database.ProjectTasks.SingleOrDefault(tp => tp.Id == id);
        }

        public bool Create(ProjectTask task)
        {
            if (_database.ProjectTasks.SingleOrDefault(t => t.Title == task.Title) != null)
            {
                return false;
            }

            _database.ProjectTasks.Add(task);
            _database.SaveChanges();

            return true;
        }

        public bool Update(int id, ProjectTask task)
        {
            ProjectTask updateTask = _database.ProjectTasks.Find(id);

            if (updateTask == null)
            {
                return false;
            }

            updateTask.Title = task.Title;

            _database.ProjectTasks.Update(updateTask);
            _database.SaveChanges();

            return true;
        }

        public bool Delete(int id)
        {
            ProjectTask removeTask = _database.ProjectTasks.Find(id);

            if (removeTask == null)
            {
                return false;
            }

            _database.ProjectTasks.Remove(removeTask);
            _database.SaveChanges();

            return true;
        }

        public bool AssignTaskToUser(int taskId, int assigneUserId, int creatorId)
        {
            var assignTask = _database.ProjectTasks.SingleOrDefault(t => t.Id == taskId);

            if(assignTask == null)
            {
                return false;
            }
            if(assignTask.TaskCreatorId != creatorId)
            {
                return false;
            }
            if(assignTask.AssignedUserId != assigneUserId)
            {
                assignTask.AssignedUserId = assigneUserId;
                _database.SaveChanges();
            }
            return true;
        }

        public bool CompleteTask(int taskId, int userId,int assignUserId)
        {
            var task = _database.ProjectTasks.SingleOrDefault(t => t.Id == taskId);
            if(task == null)
            {
                return false;
            }
            if(task.TaskCreatorId != userId && task.AssignedUserId != assignUserId)
            {
                return false;
            }

            task.Status = Status.Complete;
            _database.SaveChanges();
            return true;
        }
    }
}
