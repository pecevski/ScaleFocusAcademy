﻿using Microsoft.EntityFrameworkCore;
using ProjectManagement.Data;
using ProjectManagement.Models.Entities;
using ProjectManagement.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectManagement.Services
{
    public class UserManagement : IUserManagement
    {
        private readonly PMDataBaseContext _database;
        public static User currentUser = new();
        public UserManagement(PMDataBaseContext database)
        {
            _database = database;
        }
        public User LogIn(string username, string password)
        {
            return _database.Users.SingleOrDefault(u => u.Username == username && u.Password == password);
        }

        public IEnumerable<User> GetAll()
        {
            return _database.Users.ToList();
        }

        public User GetById(int userId)
        {
            return _database.Users.SingleOrDefault(u => u.Id == userId);
        }

        public bool Create(User user)
        {
            if (_database.Users.SingleOrDefault(u => u.Username == user.Username) != null)
            {
                return false;
            }

            _database.Users.Add(user);
            _database.SaveChanges();

            return true;
        }
        public bool Update(int id, User user)
        {
            User editingUser = _database.Users.Find(id);

            if (editingUser == null)
            {
                return false;
            }

            editingUser.Username = user.Username;
            editingUser.Password = user.Password;
            editingUser.FirstName = user.FirstName;
            editingUser.LastName = user.LastName;

            _database.Users.Update(editingUser);
            _database.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            User removeUser = _database.Users.Find(id);

            if (removeUser == null)
            {
                return false;
            }
            else if (removeUser.Id == currentUser.Id)
            {
                return false;
            }

            _database.Users.Remove(removeUser);
            _database.SaveChanges();
            return true;
        }
    }
}
