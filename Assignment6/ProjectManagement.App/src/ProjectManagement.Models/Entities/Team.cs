﻿using System.Collections.Generic;

namespace ProjectManagement.Models.Entities
{
    public class Team : Entity
    {
        public Team() : base()
        {
            TeamMembers = new List<User>();
        }
        public string TeamName { get; set; }
        public virtual User TeamCreator { get; set; }
        public virtual List<User> TeamMembers { get; set; }
        public virtual Project AssignedProject { get; set; }
        public int? ProjectId { get; set; }
        public int TeamCreatorId { get; set; }
    }
}
