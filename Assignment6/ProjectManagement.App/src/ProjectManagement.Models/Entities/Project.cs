﻿using System.Collections.Generic;

namespace ProjectManagement.Models.Entities
{
    public class Project : Entity
    {
        public Project() : base()
        {
            ProjectTasks = new List<ProjectTask>();
            Teams = new List<Team>();
        }
        public string Title { get; set; }
        public virtual User Owner { get; set; }
        public virtual List<ProjectTask> ProjectTasks { get; set; }
        public virtual List<Team> Teams { get; set; }
        public int OwnerId { get; set; }
    }
}
