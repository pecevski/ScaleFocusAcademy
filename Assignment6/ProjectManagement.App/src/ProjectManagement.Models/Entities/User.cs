﻿using ProjectManagement.Models.Enums;
using System.Collections.Generic;

namespace ProjectManagement.Models.Entities
{

    public class User : Entity
    {
        public User() : base()
        {
            CreatedProjects = new List<Project>();
            ProjectTasks = new List<ProjectTask>();
            CreatedTasks = new List<ProjectTask>();
            Teams = new List<Team>();
            CreatedTeams = new List<Team>();
            WorkLogs = new List<WorkLog>();
        }

        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Role Role { get; set; }
        public virtual List<Project> CreatedProjects { get; set; }
        public virtual List<Team> Teams { get; set; }
        public virtual List<Team> CreatedTeams { get; set; }
        public virtual List<ProjectTask> ProjectTasks { get; set; }
        public virtual List<ProjectTask> CreatedTasks { get; set; }
        public virtual List<WorkLog> WorkLogs { get; set; }
    }
}
