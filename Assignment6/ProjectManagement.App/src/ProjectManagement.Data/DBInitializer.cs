﻿using ProjectManagement.Models.Entities;
using ProjectManagement.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Data
{
    public class DBInitializer : IDBInitializer
    {
        private readonly PMDataBaseContext context;

        public DBInitializer(PMDataBaseContext context)
        {
            this.context = context;
        }
        public void Initialize()
        {
            if (context.Database.EnsureCreated())
            {
                SeedAdmin();
            }
        }

        public void SeedAdmin()
        {
            User admin = new User()
            {
                Username = "admin",
                Password = "adminpass",
                FirstName = "admin",
                LastName = "admin",
                Role = Role.Admin,
            };

            context.Add(admin);
            context.SaveChanges();
        }
    }
}
