﻿namespace ProjectManagement.Data
{
    public interface IDBInitializer
    {
        void Initialize();
        void SeedAdmin();
    }
}